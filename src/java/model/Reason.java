package model;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public enum Reason {

	offside("Offside"),
	icing("Icing"),
	goalKnockedOut("Goal knocked out"),
	puckOutOfPlay("Puck out of play"),
	penalty("Penalty"),
	puckMovementViolation("Puck movement violation");

	private String value;

	private Reason(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
