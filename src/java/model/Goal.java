package model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@Entity
public class Goal extends Event implements Serializable {

	private static final long serialVersionUID = 1L;

	@OneToOne
	private Player player;
	@OneToOne
	private Team team;

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	@Override
	public String toString() {
		return super.toString() + ": Goal: " + player.getName() + " for " + team.getName();
	}
}
