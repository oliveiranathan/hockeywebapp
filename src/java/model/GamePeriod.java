package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Temporal;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@Entity
public class GamePeriod implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private long id;
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Calendar startTime = new GregorianCalendar();
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Calendar endTime = new GregorianCalendar();
	@OneToMany
	@OrderBy("timeEvent")
	private List<Event> events = new ArrayList<>(10);

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@SuppressWarnings("ReturnOfDateField")
	public Calendar getStartTime() {
		return startTime;
	}

	@SuppressWarnings("AssignmentToDateFieldFromParameter")
	public void setStartTime(Calendar startTime) {
		this.startTime = startTime;
	}

	@SuppressWarnings("ReturnOfDateField")
	public Calendar getEndTime() {
		return endTime;
	}

	@SuppressWarnings("AssignmentToDateFieldFromParameter")
	public void setEndTime(Calendar endTime) {
		this.endTime = endTime;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<Event> getEvents() {
		return events;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setEvents(List<Event> events) {
		this.events = events;
	}
}
