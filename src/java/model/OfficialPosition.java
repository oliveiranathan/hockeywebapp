package model;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public enum OfficialPosition {

	referee("Referee"),
	linesman("Linesman"),
	goalJudge("Goal judge"),
	videoGoalJudge("Video goal judge"),
	scorer("Scorer"),
	penaltyTimekeeper("Penalty timekeeper"),
	gameTimekeeper("Game timekeeper"),
	statiscian("Statiscian");

	private String value;

	private OfficialPosition(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
