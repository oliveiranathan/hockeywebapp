package model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@Entity
public class Overtime extends GamePeriod implements Serializable {

	private static final long serialVersionUID = 1L;

	@Enumerated(EnumType.STRING)
	private TieBreakingMethod method;

	public TieBreakingMethod getMethod() {
		return method;
	}

	public void setMethod(TieBreakingMethod method) {
		this.method = method;
	}
}
