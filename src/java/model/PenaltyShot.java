package model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@Entity
public class PenaltyShot extends Event implements Serializable {

	private static final long serialVersionUID = 1L;

	@OneToOne
	private Player player;
	@Enumerated(EnumType.STRING)
	private StartPosition position;

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public StartPosition getPosition() {
		return position;
	}

	public void setPosition(StartPosition position) {
		this.position = position;
	}

	@Override
	public String toString() {
		return super.toString() + ": PenaltyShot@" + position.getValue() + ": " + player.getName();
	}
}
