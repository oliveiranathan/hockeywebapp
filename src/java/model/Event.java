package model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@Entity
public abstract class Event implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private long id;
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Calendar timeEvent = new GregorianCalendar();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@SuppressWarnings("ReturnOfDateField")
	public Calendar getTimeEvent() {
		return timeEvent;
	}

	@SuppressWarnings("AssignmentToDateFieldFromParameter")
	public void setTimeEvent(Calendar timeEvent) {
		this.timeEvent = timeEvent;
	}

	@Override
	public String toString() {
		return new SimpleDateFormat("HH:mm:ss").format(timeEvent.getTime());
	}
}
