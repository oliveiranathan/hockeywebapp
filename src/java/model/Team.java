package model;

import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@Entity
public class Team implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private String name;
	@Temporal(javax.persistence.TemporalType.DATE)
	private Calendar since = new GregorianCalendar();
	@ElementCollection
	private List<Color> colors = new ArrayList<>(10);
	@OneToOne
	private Arena home;
	@OneToOne
	private Coach coach;
	@OneToMany
	private List<Player> players = new ArrayList<>(10);

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@SuppressWarnings("ReturnOfDateField")
	public Calendar getSince() {
		return since;
	}

	@SuppressWarnings("AssignmentToDateFieldFromParameter")
	public void setSince(Calendar since) {
		this.since = since;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<Color> getColors() {
		return colors;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setColors(List<Color> colors) {
		this.colors = colors;
	}

	public Arena getHome() {
		return home;
	}

	public void setHome(Arena home) {
		this.home = home;
	}

	public Coach getCoach() {
		return coach;
	}

	public void setCoach(Coach coach) {
		this.coach = coach;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<Player> getPlayers() {
		return players;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	@Override
	public String toString() {
		return name;
	}
}
