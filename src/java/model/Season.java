package model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Temporal;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@Entity
public class Season implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private long id;
	@Temporal(javax.persistence.TemporalType.DATE)
	private Calendar startDate = new GregorianCalendar();
	@Temporal(javax.persistence.TemporalType.DATE)
	private Calendar endDate = new GregorianCalendar();
	@OneToMany
	@OrderBy("startTime")
	private List<Game> games = new ArrayList<>(10);

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@SuppressWarnings("ReturnOfDateField")
	public Calendar getStartDate() {
		return startDate;
	}

	@SuppressWarnings("AssignmentToDateFieldFromParameter")
	public void setStartDate(Calendar startDate) {
		this.startDate = startDate;
	}

	@SuppressWarnings("ReturnOfDateField")
	public Calendar getEndDate() {
		return endDate;
	}

	@SuppressWarnings("AssignmentToDateFieldFromParameter")
	public void setEndDate(Calendar endDate) {
		this.endDate = endDate;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<Game> getGames() {
		return games;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setGames(List<Game> games) {
		this.games = games;
	}

	@Override
	public String toString() {
		return new SimpleDateFormat("yyyy-MM-dd").format(startDate.getTime()) + " - " + new SimpleDateFormat("yyyy-MM-dd").format(endDate.getTime());
	}
}
