package model;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public enum PenaltyType {

	boarding("Boarding"),
	charging("Charging"),
	crossChecking("Cross checking"),
	delaying("Delaying"),
	elbowing("Elbowing"),
	handPass("Hand pass"),
	highSticking("High sticking"),
	hittingFromBehind("Hitting from behind"),
	holding("Holding"),
	hooking("Hooking"),
	interference("Interference"),
	slashing("Slashing"),
	spearing("Spearing"),
	tripping("Tripping"),
	roughing("Roughing"),
	tooManyMenOnTheIce("Too many men on the ice"),
	fighting("Fighting"),
	hitToTheHead("Hit to the head"),
	unsportsmanlikeConduct("Unsportsmanlike conduct");

	private String value;

	private PenaltyType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
