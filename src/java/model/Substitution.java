package model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@Entity
public class Substitution extends Event implements Serializable {

	private static final long serialVersionUID = 1L;

	@OneToOne
	private Player in;
	@OneToOne
	private Player out;

	public Player getIn() {
		return in;
	}

	public void setIn(Player in) {
		this.in = in;
	}

	public Player getOut() {
		return out;
	}

	public void setOut(Player out) {
		this.out = out;
	}

	@Override
	public String toString() {
		return super.toString() + ": Substitution: IN: " + in.getName() + " OUT: " + out.getName();
	}
}
