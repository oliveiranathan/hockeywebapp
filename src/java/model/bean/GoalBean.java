/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.Event;
import model.GamePeriod;
import model.Goal;
import model.Overtime;
import model.Team;
import model.dao.DataSource;
import model.dao.GamePeriodDAO;
import model.dao.GoalDAO;
import model.dao.OvertimeDAO;
import model.dao.PlayerDAO;
import model.dao.TeamDAO;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@ManagedBean
@ViewScoped
public class GoalBean {

	private final GoalDAO goalDAO = new GoalDAO(DataSource.createEntityManager());
	private final PlayerDAO playerDAO = new PlayerDAO(DataSource.createEntityManager());
	private final TeamDAO teamDAO = new TeamDAO(DataSource.createEntityManager());
	private Goal goal = new Goal();
	private List<Goal> goals = goalDAO.findAll();
	private long selected = -1;
	private String teamName = new String();
	private String playerTeamName = new String();
	private long player = -1;
	private Team team = new Team();
	private boolean enableEditing;

	public String insert() {
		goal.setPlayer(playerDAO.find(player));
		goal.setTeam(teamDAO.find(teamName));
		goalDAO.create(goal);
		return "/index";
	}

	public String find() {
		goal = goalDAO.find(selected);
		teamName = goal.getTeam().getName();
		player = goal.getPlayer().getId();
		team = teamDAO.findTeamOf(goal.getPlayer());
		playerTeamName = team.getName();
		return "";
	}

	public String update() {
		goal.setPlayer(playerDAO.find(player));
		goal.setTeam(teamDAO.find(teamName));
		goalDAO.update(goal);
		return "/index";
	}

	public String delete() {
		GamePeriodDAO gamePeriodDAO = new GamePeriodDAO(DataSource.createEntityManager());
		GamePeriod gp = gamePeriodDAO.findPeriodOf(goal);
		if (gp != null) {
			List<Event> loe = gp.getEvents();
			for (int i = 0; i < loe.size(); i++) {
				if (loe.get(i).getId() == goal.getId()) {
					loe.remove(i);
					break;
				}
			}
			gp.setEvents(new ArrayList<>(loe));
			gamePeriodDAO.update(gp);
		} else {
			OvertimeDAO overtimeDAO = new OvertimeDAO(DataSource.createEntityManager());
			Overtime o = overtimeDAO.findOvertimeOf(goal);
			if (o != null) {
				List<Event> loe = o.getEvents();
				for (int i = 0; i < loe.size(); i++) {
					if (loe.get(i).getId() == goal.getId()) {
						loe.remove(i);
						break;
					}
				}
				o.setEvents(new ArrayList<>(loe));
				overtimeDAO.update(o);
			}
		}
		goalDAO.delete(goal);
		return "/index";
	}

	public void playerTeamChanged() {
		if (playerTeamName == null || playerTeamName.isEmpty()) {
			team = new Team();
		} else {
			team = teamDAO.find(playerTeamName);
		}
	}

	public Goal getGoal() {
		return goal;
	}

	public void setGoal(Goal goal) {
		this.goal = goal;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<Goal> getGoals() {
		return goals;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setGoals(List<Goal> goals) {
		this.goals = goals;
	}

	public long getSelected() {
		return selected;
	}

	public void setSelected(long selected) {
		this.selected = selected;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public long getPlayer() {
		return player;
	}

	public void setPlayer(long player) {
		this.player = player;
	}

	public String getPlayerTeamName() {
		return playerTeamName;
	}

	public void setPlayerTeamName(String playerTeamName) {
		this.playerTeamName = playerTeamName;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public boolean isEnableEditing() {
		return enableEditing;
	}

	public void setEnableEditing(boolean enableEditing) {
		this.enableEditing = enableEditing;
	}
}
