/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.Game;
import model.Official;
import model.Season;
import model.dao.DataSource;
import model.dao.GameDAO;
import model.dao.OfficialDAO;
import model.dao.SeasonDAO;
import model.dao.TeamDAO;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@ManagedBean
@ViewScoped
public class GameBean {

	private final GameDAO gameDAO = new GameDAO(DataSource.createEntityManager());
	private final OfficialDAO officialDAO = new OfficialDAO(DataSource.createEntityManager());
	private final TeamDAO teamDAO = new TeamDAO(DataSource.createEntityManager());
	private final SeasonDAO seasonDAO = new SeasonDAO(DataSource.createEntityManager());
	private Game game = new Game();
	private List<Game> games = gameDAO.findAll();
	private long selected = -1;
	private String homeTeam = new String();
	private String opposingTeam = new String();
	private long season;
	private List<String> selectedOfficials = new ArrayList<>(10);
	private boolean enableEditing;

	public String insert() {
		List<Official> os = new ArrayList<>(selectedOfficials.size());
		for (String l : selectedOfficials) {
			os.add(officialDAO.find(Long.parseLong(l)));
		}
		game.setOfficials(os);
		game.setHomeTeam(teamDAO.find(homeTeam));
		game.setOpposingTeam(teamDAO.find(opposingTeam));
		gameDAO.create(game);
		Season s = seasonDAO.find(season);
		if (s != null) {
			List<Game> gs = new ArrayList<>(s.getGames());
			gs.add(game);
			s.setGames(gs);
			seasonDAO.update(s);
		}
		return "/index";
	}

	public String find() {
		game = gameDAO.find(selected);
		selectedOfficials = new ArrayList<>(game.getOfficials().size());
		for (Official o : game.getOfficials()) {
			selectedOfficials.add(Long.toString(o.getId()));
		}
		homeTeam = game.getHomeTeam().getName();
		opposingTeam = game.getOpposingTeam().getName();
		season = seasonDAO.findSeasonOf(game).getId();
		return "";
	}

	public String update() {
		List<Official> os = new ArrayList<>(selectedOfficials.size());
		for (String l : selectedOfficials) {
			os.add(officialDAO.find(Long.parseLong(l)));
		}
		game.setOfficials(os);
		game.setHomeTeam(teamDAO.find(homeTeam));
		game.setOpposingTeam(teamDAO.find(opposingTeam));
		gameDAO.update(game);
		Season s = seasonDAO.find(season);
		if (s != null) {
			List<Game> gs = new ArrayList<>(s.getGames());
			gs.add(game);
			s.setGames(gs);
			seasonDAO.update(s);
		}
		return "/index";
	}

	public String delete() {
		Season s = seasonDAO.findSeasonOf(game);
		if (s != null) {
			List<Game> gs = new ArrayList<>(s.getGames());
			for (int i = 0; i < gs.size(); i++) {
				if (gs.get(i).getId() == game.getId()) {
					gs.remove(i);
					break;
				}
			}
			s.setGames(gs);
			seasonDAO.update(s);
		}
		gameDAO.delete(game);
		return "/index";
	}

	public String officialName(long oId) {
		return officialDAO.find(oId).getName();
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<Game> getGames() {
		return games;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setGames(List<Game> games) {
		this.games = games;
	}

	public long getSelected() {
		return selected;
	}

	public void setSelected(long selected) {
		this.selected = selected;
	}

	public String getHomeTeam() {
		return homeTeam;
	}

	public void setHomeTeam(String homeTeam) {
		this.homeTeam = homeTeam;
	}

	public String getOpposingTeam() {
		return opposingTeam;
	}

	public void setOpposingTeam(String opposingTeam) {
		this.opposingTeam = opposingTeam;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<String> getSelectedOfficials() {
		return selectedOfficials;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setSelectedOfficials(List<String> selectedOfficials) {
		this.selectedOfficials = selectedOfficials;
	}

	public long getSeason() {
		return season;
	}

	public void setSeason(long season) {
		this.season = season;
	}

	public boolean isEnableEditing() {
		return enableEditing;
	}

	public void setEnableEditing(boolean enableEditing) {
		this.enableEditing = enableEditing;
	}
}
