/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.Event;
import model.GamePeriod;
import model.Overtime;
import model.Substitution;
import model.Team;
import model.dao.DataSource;
import model.dao.GamePeriodDAO;
import model.dao.OvertimeDAO;
import model.dao.PlayerDAO;
import model.dao.SubstitutionDAO;
import model.dao.TeamDAO;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@ManagedBean
@ViewScoped
public class SubstitutionBean {

	private final SubstitutionDAO substitutionDAO = new SubstitutionDAO(DataSource.createEntityManager());
	private final TeamDAO teamDAO = new TeamDAO(DataSource.createEntityManager());
	private final PlayerDAO playerDAO = new PlayerDAO(DataSource.createEntityManager());
	private Substitution substitution = new Substitution();
	private List<Substitution> substitutions = substitutionDAO.findAll();
	private long selected = -1;
	private String teamName = new String();
	private Team team = new Team();
	private long p1 = -1;
	private long p2 = -1;
	private boolean enableEditing;

	public String insert() {
		substitution.setIn(playerDAO.find(p1));
		substitution.setOut(playerDAO.find(p2));
		substitutionDAO.create(substitution);
		return "/index";
	}

	public String find() {
		substitution = substitutionDAO.find(selected);
		team = teamDAO.findTeamOf(substitution.getIn());
		p1 = substitution.getIn().getId();
		p2 = substitution.getOut().getId();
		teamName = team.getName();
		return "";
	}

	public String update() {
		substitution.setIn(playerDAO.find(p1));
		substitution.setOut(playerDAO.find(p2));
		substitutionDAO.update(substitution);
		return "/index";
	}

	public String delete() {
		GamePeriodDAO gamePeriodDAO = new GamePeriodDAO(DataSource.createEntityManager());
		GamePeriod gp = gamePeriodDAO.findPeriodOf(substitution);
		if (gp != null) {
			List<Event> loe = gp.getEvents();
			for (int i = 0; i < loe.size(); i++) {
				if (loe.get(i).getId() == substitution.getId()) {
					loe.remove(i);
					break;
				}
			}
			gp.setEvents(new ArrayList<>(loe));
			gamePeriodDAO.update(gp);
		} else {
			OvertimeDAO overtimeDAO = new OvertimeDAO(DataSource.createEntityManager());
			Overtime o = overtimeDAO.findOvertimeOf(substitution);
			if (o != null) {
				List<Event> loe = o.getEvents();
				for (int i = 0; i < loe.size(); i++) {
					if (loe.get(i).getId() == substitution.getId()) {
						loe.remove(i);
						break;
					}
				}
				o.setEvents(new ArrayList<>(loe));
				overtimeDAO.update(o);
			}
		}
		substitutionDAO.delete(substitution);
		return "/index";
	}

	public void teamChanged() {
		if (teamName == null || teamName.isEmpty()) {
			team = new Team();
		} else {
			team = teamDAO.find(teamName);
		}
	}

	public Substitution getSubstitution() {
		return substitution;
	}

	public void setSubstitution(Substitution substitution) {
		this.substitution = substitution;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<Substitution> getSubstitutions() {
		return substitutions;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setSubstitutions(List<Substitution> substitutions) {
		this.substitutions = substitutions;
	}

	public long getSelected() {
		return selected;
	}

	public void setSelected(long selected) {
		this.selected = selected;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public long getP1() {
		return p1;
	}

	public void setP1(long p1) {
		this.p1 = p1;
	}

	public long getP2() {
		return p2;
	}

	public void setP2(long p2) {
		this.p2 = p2;
	}

	public boolean isEnableEditing() {
		return enableEditing;
	}

	public void setEnableEditing(boolean enableEditing) {
		this.enableEditing = enableEditing;
	}
}
