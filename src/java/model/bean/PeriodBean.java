/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.Event;
import model.Game;
import model.GamePeriod;
import model.dao.DataSource;
import model.dao.EventDAO;
import model.dao.GameDAO;
import model.dao.GamePeriodDAO;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@ManagedBean
@ViewScoped
public class PeriodBean {

	private final GamePeriodDAO periodDAO = new GamePeriodDAO(DataSource.createEntityManager());
	private final GameDAO gameDAO = new GameDAO(DataSource.createEntityManager());
	private final EventDAO eventDAO = new EventDAO(DataSource.createEntityManager());
	private GamePeriod period = new GamePeriod();
	private List<GamePeriod> periods = periodDAO.findAll();
	private long selected = -1;
	private long game;
	private List<String> events = new ArrayList<>(10);
	private boolean enableEditing;

	public String insert() {
		List<Event> es = new ArrayList<>(events.size());
		for (String e : events) {
			es.add(eventDAO.find(Long.parseLong(e)));
		}
		for (Event e : es) {
			GamePeriod gp = periodDAO.findPeriodOf(e);
			if (gp != null) {
				List<Event> oEs = new ArrayList<>(gp.getEvents());
				for (int i = 0; i < oEs.size(); i++) {
					if (oEs.get(i).getId() == e.getId()) {
						oEs.remove(i);
						break;
					}
				}
				gp.setEvents(oEs);
				periodDAO.update(gp);
			}
		}
		period.setEvents(es);
		periodDAO.create(period);
		Game g = gameDAO.find(game);
		if (g != null) {
			List<GamePeriod> gps = new ArrayList<>(g.getPeriods());
			gps.add(period);
			g.setPeriods(gps);
			gameDAO.update(g);
		}
		return "/index";
	}

	public String find() {
		period = periodDAO.find(selected);
		game = gameDAO.findGameOf(period).getId();
		events = new ArrayList<>(period.getEvents().size());
		for (Event e : period.getEvents()) {
			events.add(Long.toString(e.getId()));
		}
		return "";
	}

	public String update() {
		Game g = gameDAO.findGameOf(period);
		if (g != null) {
			List<GamePeriod> gps = new ArrayList<>(g.getPeriods());
			for (int i = 0; i < gps.size(); i++) {
				if (gps.get(i).getId() == period.getId()) {
					gps.remove(i);
					break;
				}
			}
			g.setPeriods(gps);
			gameDAO.update(g);
		}
		List<Event> es = new ArrayList<>(events.size());
		for (String e : events) {
			es.add(eventDAO.find(Long.parseLong(e)));
		}
		for (Event e : es) {
			GamePeriod gp = periodDAO.findPeriodOf(e);
			if (gp != null) {
				List<Event> oEs = new ArrayList<>(gp.getEvents());
				for (int i = 0; i < oEs.size(); i++) {
					if (oEs.get(i).getId() == e.getId()) {
						oEs.remove(i);
						break;
					}
				}
				gp.setEvents(oEs);
				periodDAO.update(gp);
			}
		}
		period.setEvents(es);
		periodDAO.update(period);
		g = gameDAO.find(game);
		if (g != null) {
			List<GamePeriod> gps = new ArrayList<>(g.getPeriods());
			gps.add(period);
			g.setPeriods(gps);
			gameDAO.update(g);
		}
		return "/index";
	}

	public String delete() {
		Game g = gameDAO.findGameOf(period);
		if (g != null) {
			List<GamePeriod> gps = new ArrayList<>(g.getPeriods());
			for (int i = 0; i < gps.size(); i++) {
				if (gps.get(i).getId() == period.getId()) {
					gps.remove(i);
					break;
				}
			}
			g.setPeriods(gps);
			gameDAO.update(g);
		}
		periodDAO.delete(period);
		return "/index";
	}

	public String eventLabel(long eId) {
		return eventDAO.find(eId).toString();
	}

	public GamePeriod getPeriod() {
		return period;
	}

	public void setPeriod(GamePeriod period) {
		this.period = period;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<GamePeriod> getPeriods() {
		return periods;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setPeriods(List<GamePeriod> periods) {
		this.periods = periods;
	}

	public long getSelected() {
		return selected;
	}

	public void setSelected(long selected) {
		this.selected = selected;
	}

	public long getGame() {
		return game;
	}

	public void setGame(long game) {
		this.game = game;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<String> getEvents() {
		return events;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setEvents(List<String> events) {
		this.events = events;
	}

	public boolean isEnableEditing() {
		return enableEditing;
	}

	public void setEnableEditing(boolean enableEditing) {
		this.enableEditing = enableEditing;
	}
}
