/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.League;
import model.Season;
import model.dao.DataSource;
import model.dao.GameDAO;
import model.dao.LeagueDAO;
import model.dao.SeasonDAO;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@ManagedBean
@ViewScoped
public class SeasonBean {

	private final SeasonDAO seasonDAO = new SeasonDAO(DataSource.createEntityManager());
	private final GameDAO gameDAO = new GameDAO(DataSource.createEntityManager());
	private Season season = new Season();
	private List<Season> seasons = seasonDAO.findAll();
	private long selected = -1;
	private boolean enableEditing;

	public String insert() {
		seasonDAO.create(season);
		return "/index";
	}

	public String find() {
		season = seasonDAO.find(selected);
		return "";
	}

	public String update() {
		seasonDAO.update(season);
		return "/index";
	}

	public String delete() {
		LeagueDAO leagueDAO = new LeagueDAO(DataSource.createEntityManager());
		League league = leagueDAO.findLeagueOf(season);
		if (league != null) {
			List<Season> ss = league.getSeasons();
			for (int i = 0; i < ss.size(); i++) {
				if (ss.get(i).getId() == season.getId()) {
					ss.remove(i);
					break;
				}
			}
			league.setSeasons(new ArrayList<>(ss));
			leagueDAO.update(league);
		}
		seasonDAO.delete(season);
		return "/index";
	}

	public String gameLabel(long gId) {
		return gameDAO.find(gId).toString();
	}

	public Season getSeason() {
		return season;
	}

	public void setSeason(Season season) {
		this.season = season;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<Season> getSeasons() {
		return seasons;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setSeasons(List<Season> seasons) {
		this.seasons = seasons;
	}

	public long getSelected() {
		return selected;
	}

	public void setSelected(long selected) {
		this.selected = selected;
	}

	public boolean isEnableEditing() {
		return enableEditing;
	}

	public void setEnableEditing(boolean enableEditing) {
		this.enableEditing = enableEditing;
	}
}
