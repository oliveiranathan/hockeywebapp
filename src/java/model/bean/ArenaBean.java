/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.Arena;
import model.Team;
import model.dao.ArenaDAO;
import model.dao.DataSource;
import model.dao.TeamDAO;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@ManagedBean
@ViewScoped
public class ArenaBean {

	private final ArenaDAO arenaDAO = new ArenaDAO(DataSource.createEntityManager());
	private final TeamDAO teamDAO = new TeamDAO(DataSource.createEntityManager());
	private Arena arena = new Arena();
	private List<Arena> arenas = arenaDAO.findAll();
	private String selected;
	private boolean enableEditing;
	private boolean enableKey;

	public String insert() {
		arenaDAO.create(arena);
		return "/index";
	}

	public String update() {
		arenaDAO.update(arena);
		return "/index";
	}

	public String find() {
		arena = arenaDAO.find(selected);
		return "";
	}

	public String delete() {
		Team t = teamDAO.findTeamOf(arena);
		if (t != null) {
			t.setHome(null);
			teamDAO.update(t);
		}
		arenaDAO.delete(arena);
		return "/index";
	}

	public Arena getArena() {
		return arena;
	}

	public void setArena(Arena arena) {
		this.arena = arena;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<Arena> getArenas() {
		return arenas;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setArenas(List<Arena> arenas) {
		this.arenas = arenas;
	}

	public String getSelected() {
		return selected;
	}

	public void setSelected(String selected) {
		this.selected = selected;
	}

	public boolean isEnableEditing() {
		return enableEditing;
	}

	public void setEnableEditing(boolean enableEditing) {
		this.enableEditing = enableEditing;
	}

	public boolean isEnableKey() {
		return enableKey;
	}

	public void setEnableKey(boolean enableKey) {
		this.enableKey = enableKey;
	}
}
