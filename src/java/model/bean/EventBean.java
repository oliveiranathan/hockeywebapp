/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.Event;
import model.dao.DataSource;
import model.dao.EventDAO;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@ManagedBean
@ViewScoped
public class EventBean {

	private List<Event> events = new EventDAO(DataSource.createEntityManager()).findAll();

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<Event> getEvents() {
		return events;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setEvents(List<Event> events) {
		this.events = events;
	}
}
