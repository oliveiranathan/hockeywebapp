/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.Event;
import model.Game;
import model.GamePeriod;
import model.Overtime;
import model.TieBreakingMethod;
import model.dao.DataSource;
import model.dao.EventDAO;
import model.dao.GameDAO;
import model.dao.GamePeriodDAO;
import model.dao.OvertimeDAO;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@ManagedBean
@ViewScoped
public class OvertimeBean {

	private final OvertimeDAO overtimeDAO = new OvertimeDAO(DataSource.createEntityManager());
	private final GamePeriodDAO periodDAO = new GamePeriodDAO(DataSource.createEntityManager());
	private final GameDAO gameDAO = new GameDAO(DataSource.createEntityManager());
	private final EventDAO eventDAO = new EventDAO(DataSource.createEntityManager());
	private Overtime overtime = new Overtime();
	private List<Overtime> overtimes = overtimeDAO.findAll();
	private long selected = -1;
	private long game;
	private TieBreakingMethod[] methods = TieBreakingMethod.values();
	private List<String> events = new ArrayList<>(10);
	private boolean enableEditing;

	public String insert() {
		List<Event> es = new ArrayList<>(events.size());
		for (String e : events) {
			es.add(eventDAO.find(Long.parseLong(e)));
		}
		for (Event e : es) {
			GamePeriod gp = periodDAO.findPeriodOf(e);
			if (gp != null) {
				List<Event> oEs = new ArrayList<>(gp.getEvents());
				for (int i = 0; i < oEs.size(); i++) {
					if (oEs.get(i).getId() == e.getId()) {
						oEs.remove(i);
						break;
					}
				}
				gp.setEvents(oEs);
				periodDAO.update(gp);
			}
		}
		overtime.setEvents(es);
		overtimeDAO.create(overtime);
		Game g = gameDAO.find(game);
		if (g != null) {
			List<GamePeriod> gps = new ArrayList<>(g.getPeriods());
			gps.add(overtime);
			g.setPeriods(gps);
			gameDAO.update(g);
		}
		return "/index";
	}

	public String find() {
		overtime = overtimeDAO.find(selected);
		game = gameDAO.findGameOf(overtime).getId();
		events = new ArrayList<>(overtime.getEvents().size());
		for (Event e : overtime.getEvents()) {
			events.add(Long.toString(e.getId()));
		}
		return "";
	}

	public String update() {
		Game g = gameDAO.findGameOf(overtime);
		if (g != null) {
			List<GamePeriod> gps = new ArrayList<>(g.getPeriods());
			for (int i = 0; i < gps.size(); i++) {
				if (gps.get(i).getId() == overtime.getId()) {
					gps.remove(i);
					break;
				}
			}
			g.setPeriods(gps);
			gameDAO.update(g);
		}
		List<Event> es = new ArrayList<>(events.size());
		for (String e : events) {
			es.add(eventDAO.find(Long.parseLong(e)));
		}
		for (Event e : es) {
			GamePeriod gp = periodDAO.findPeriodOf(e);
			if (gp != null) {
				List<Event> oEs = new ArrayList<>(gp.getEvents());
				for (int i = 0; i < oEs.size(); i++) {
					if (oEs.get(i).getId() == e.getId()) {
						oEs.remove(i);
						break;
					}
				}
				gp.setEvents(oEs);
				periodDAO.update(gp);
			}
		}
		overtime.setEvents(es);
		overtimeDAO.update(overtime);
		g = gameDAO.find(game);
		if (g != null) {
			List<GamePeriod> gps = new ArrayList<>(g.getPeriods());
			gps.add(overtime);
			g.setPeriods(gps);
			gameDAO.update(g);
		}
		return "/index";
	}

	public String delete() {
		Game g = gameDAO.findGameOf(overtime);
		if (g != null) {
			List<GamePeriod> gps = new ArrayList<>(g.getPeriods());
			for (int i = 0; i < gps.size(); i++) {
				if (gps.get(i).getId() == overtime.getId()) {
					gps.remove(i);
					break;
				}
			}
			g.setPeriods(gps);
			gameDAO.update(g);
		}
		overtimeDAO.delete(overtime);
		return "/index";
	}

	public Overtime getOvertime() {
		return overtime;
	}

	public void setOvertime(Overtime overtime) {
		this.overtime = overtime;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<Overtime> getOvertimes() {
		return overtimes;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setOvertimes(List<Overtime> overtimes) {
		this.overtimes = overtimes;
	}

	public long getSelected() {
		return selected;
	}

	public void setSelected(long selected) {
		this.selected = selected;
	}

	public long getGame() {
		return game;
	}

	public void setGame(long game) {
		this.game = game;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public TieBreakingMethod[] getMethods() {
		return methods;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setMethods(TieBreakingMethod[] methods) {
		this.methods = methods;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<String> getEvents() {
		return events;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setEvents(List<String> events) {
		this.events = events;
	}

	public boolean isEnableEditing() {
		return enableEditing;
	}

	public void setEnableEditing(boolean enableEditing) {
		this.enableEditing = enableEditing;
	}
}
