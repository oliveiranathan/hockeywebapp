/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.League;
import model.Official;
import model.Season;
import model.Team;
import model.dao.DataSource;
import model.dao.LeagueDAO;
import model.dao.OfficialDAO;
import model.dao.SeasonDAO;
import model.dao.TeamDAO;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@ManagedBean
@ViewScoped
public class LeagueBean {

	private final LeagueDAO leagueDAO = new LeagueDAO(DataSource.createEntityManager());
	private final OfficialDAO officialDAO = new OfficialDAO(DataSource.createEntityManager());
	private final SeasonDAO seasonDAO = new SeasonDAO(DataSource.createEntityManager());
	private final TeamDAO teamDAO = new TeamDAO(DataSource.createEntityManager());
	private League league = new League();
	private List<League> leagues = leagueDAO.findAll();
	private String selected = new String();
	private List<String> selectedOfficials = new ArrayList<>(10);
	private List<String> selectedSeasons = new ArrayList<>(10);
	private List<String> selectedTeams = new ArrayList<>(10);
	private boolean enableEditing;
	private boolean enableKey;

	public String insert() {
		List<Official> os = new ArrayList<>(selectedOfficials.size());
		for (String l : selectedOfficials) {
			os.add(officialDAO.find(Long.parseLong(l)));
		}
		league.setOfficials(os);
		List<Season> ss = new ArrayList<>(selectedSeasons.size());
		for (String l : selectedSeasons) {
			ss.add(seasonDAO.find(Long.parseLong(l)));
		}
		league.setSeasons(ss);
		List<Team> ts = new ArrayList<>(selectedTeams.size());
		for (String s : selectedTeams) {
			ts.add(teamDAO.find(s));
		}
		league.setTeams(ts);
		leagueDAO.create(league);
		return "/index";
	}

	public String find() {
		league = leagueDAO.find(selected);
		selectedOfficials = new ArrayList<>(league.getOfficials().size());
		for (Official o : league.getOfficials()) {
			selectedOfficials.add(Long.toString(o.getId()));
		}
		selectedSeasons = new ArrayList<>(league.getSeasons().size());
		for (Season s : league.getSeasons()) {
			selectedSeasons.add(Long.toString(s.getId()));
		}
		selectedTeams = new ArrayList<>(league.getTeams().size());
		for (Team t : league.getTeams()) {
			selectedTeams.add(t.getName());
		}
		return "";
	}

	public String update() {
		List<Official> os = new ArrayList<>(selectedOfficials.size());
		for (String l : selectedOfficials) {
			os.add(officialDAO.find(Long.parseLong(l)));
		}
		league.setOfficials(os);
		List<Season> ss = new ArrayList<>(selectedSeasons.size());
		for (String l : selectedSeasons) {
			ss.add(seasonDAO.find(Long.parseLong(l)));
		}
		league.setSeasons(ss);
		List<Team> ts = new ArrayList<>(selectedTeams.size());
		for (String s : selectedTeams) {
			ts.add(teamDAO.find(s));
		}
		league.setTeams(ts);
		leagueDAO.update(league);
		return "/index";
	}

	public String delete() {
		for (Official o : league.getOfficials()) {
			List<League> ls = new ArrayList<>(o.getLeagues());
			for (int i = 0; i < ls.size(); i++) {
				if (ls.get(i).getName().equals(league.getName())) {
					ls.remove(i);
					break;
				}
			}
			o.setLeagues(ls);
			officialDAO.update(o);
		}
		leagueDAO.delete(league);
		return "/index";
	}

	public String officialLabel(long oId) {
		return officialDAO.find(oId).getName();
	}

	public String seasonLabel(long sId) {
		return seasonDAO.find(sId).toString();
	}

	public League getLeague() {
		return league;
	}

	public void setLeague(League league) {
		this.league = league;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<League> getLeagues() {
		return leagues;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setLeagues(List<League> leagues) {
		this.leagues = leagues;
	}

	public String getSelected() {
		return selected;
	}

	public void setSelected(String selected) {
		this.selected = selected;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<String> getSelectedOfficials() {
		return selectedOfficials;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setSelectedOfficials(List<String> selectedOfficials) {
		this.selectedOfficials = selectedOfficials;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<String> getSelectedSeasons() {
		return selectedSeasons;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setSelectedSeasons(List<String> selectedSeasons) {
		this.selectedSeasons = selectedSeasons;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<String> getSelectedTeams() {
		return selectedTeams;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setSelectedTeams(List<String> selectedTeams) {
		this.selectedTeams = selectedTeams;
	}

	public boolean isEnableEditing() {
		return enableEditing;
	}

	public void setEnableEditing(boolean enableEditing) {
		this.enableEditing = enableEditing;
	}

	public boolean isEnableKey() {
		return enableKey;
	}

	public void setEnableKey(boolean enableKey) {
		this.enableKey = enableKey;
	}
}
