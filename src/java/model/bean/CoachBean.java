/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.Coach;
import model.Team;
import model.dao.CoachDAO;
import model.dao.DataSource;
import model.dao.TeamDAO;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@ManagedBean
@ViewScoped
public class CoachBean {

	private final CoachDAO coachDAO = new CoachDAO(DataSource.createEntityManager());
	private final TeamDAO teamDAO = new TeamDAO(DataSource.createEntityManager());
	private Coach coach = new Coach();
	private List<Coach> coaches = coachDAO.findAll();
	private String teamName = new String();
	private long selected = -1;
	private boolean enableEditing;

	public String insert() {
		Team t = teamDAO.find(teamName);
		if (t != null) {
			Coach oldC = t.getCoach();
			if (oldC != null) {
				oldC.setTeam(null);
				coachDAO.update(oldC);
			}
		}
		coach.setTeam(t);
		coachDAO.create(coach);
		if (t != null) {
			coach.getTeam().setCoach(coach);
			teamDAO.update(coach.getTeam());
		}
		return "/index";
	}

	public String find() {
		coach = coachDAO.find(selected);
		return "";
	}

	public String update() {
		Team t = teamDAO.find(teamName);
		coach.setTeam(t);
		Coach oldC = t.getCoach();
		if (oldC != null) {
			oldC.setTeam(null);
			coachDAO.update(oldC);
		}
		coachDAO.update(coach);
		coach.getTeam().setCoach(coach);
		teamDAO.update(coach.getTeam());
		return "/index";
	}

	public String delete() {
		Team t = coach.getTeam();
		if (t != null) {
			t.setCoach(null);
			teamDAO.update(t);
		}
		coachDAO.delete(coach);
		return "/index";
	}

	public Coach getCoach() {
		return coach;
	}

	public void setCoach(Coach coach) {
		this.coach = coach;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public long getSelected() {
		return selected;
	}

	public void setSelected(long selected) {
		this.selected = selected;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<Coach> getCoaches() {
		return coaches;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setCoaches(List<Coach> coaches) {
		this.coaches = coaches;
	}

	public boolean isEnableEditing() {
		return enableEditing;
	}

	public void setEnableEditing(boolean enableEditing) {
		this.enableEditing = enableEditing;
	}
}
