/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.Event;
import model.GamePeriod;
import model.Overtime;
import model.Reason;
import model.Stoppage;
import model.dao.DataSource;
import model.dao.GamePeriodDAO;
import model.dao.OvertimeDAO;
import model.dao.StoppageDAO;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@ManagedBean
@ViewScoped
public class StoppageBean {

	private final StoppageDAO stoppageDAO = new StoppageDAO(DataSource.createEntityManager());
	private Stoppage stoppage = new Stoppage();
	private List<Stoppage> stoppages = stoppageDAO.findAll();
	private Reason[] reasons = Reason.values();
	private long selected = -1;
	private boolean enableEditing;

	public String insert() {
		stoppageDAO.create(stoppage);
		return "/index";
	}

	public String find() {
		stoppage = stoppageDAO.find(selected);
		return "";
	}

	public String update() {
		stoppageDAO.update(stoppage);
		return "/index";
	}

	public String delete() {
		GamePeriodDAO gamePeriodDAO = new GamePeriodDAO(DataSource.createEntityManager());
		GamePeriod gp = gamePeriodDAO.findPeriodOf(stoppage);
		if (gp != null) {
			List<Event> loe = gp.getEvents();
			for (int i = 0; i < loe.size(); i++) {
				if (loe.get(i).getId() == stoppage.getId()) {
					loe.remove(i);
					break;
				}
			}
			gp.setEvents(new ArrayList<>(loe));
			gamePeriodDAO.update(gp);
		} else {
			OvertimeDAO overtimeDAO = new OvertimeDAO(DataSource.createEntityManager());
			Overtime o = overtimeDAO.findOvertimeOf(stoppage);
			if (o != null) {
				List<Event> loe = o.getEvents();
				for (int i = 0; i < loe.size(); i++) {
					if (loe.get(i).getId() == stoppage.getId()) {
						loe.remove(i);
						break;
					}
				}
				o.setEvents(new ArrayList<>(loe));
				overtimeDAO.update(o);
			}
		}
		stoppageDAO.delete(stoppage);
		return "/index";
	}

	public Stoppage getStoppage() {
		return stoppage;
	}

	public void setStoppage(Stoppage stoppage) {
		this.stoppage = stoppage;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<Stoppage> getStoppages() {
		return stoppages;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setStoppages(List<Stoppage> stoppages) {
		this.stoppages = stoppages;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public Reason[] getReasons() {
		return reasons;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setReasons(Reason[] reasons) {
		this.reasons = reasons;
	}

	public long getSelected() {
		return selected;
	}

	public void setSelected(long selected) {
		this.selected = selected;
	}

	public boolean isEnableEditing() {
		return enableEditing;
	}

	public void setEnableEditing(boolean enableEditing) {
		this.enableEditing = enableEditing;
	}
}
