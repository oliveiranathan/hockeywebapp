/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.Event;
import model.GamePeriod;
import model.Overtime;
import model.PenaltyShot;
import model.StartPosition;
import model.Team;
import model.dao.DataSource;
import model.dao.GamePeriodDAO;
import model.dao.OvertimeDAO;
import model.dao.PenaltyShotDAO;
import model.dao.PlayerDAO;
import model.dao.TeamDAO;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@ManagedBean
@ViewScoped
public class PenaltyShotBean {

	private final PenaltyShotDAO penaltyShotDAO = new PenaltyShotDAO(DataSource.createEntityManager());
	private final TeamDAO teamDAO = new TeamDAO(DataSource.createEntityManager());
	private final PlayerDAO playerDAO = new PlayerDAO(DataSource.createEntityManager());
	private PenaltyShot penaltyShot = new PenaltyShot();
	private List<PenaltyShot> penaltyShots = penaltyShotDAO.findAll();
	private StartPosition[] positions = StartPosition.values();
	private long selected = -1;
	private String teamName = new String();
	private Team team = new Team();
	private long player = -1;
	private boolean enableEditing;

	public String insert() {
		penaltyShot.setPlayer(playerDAO.find(player));
		penaltyShotDAO.create(penaltyShot);
		return "/index";
	}

	public String find() {
		penaltyShot = penaltyShotDAO.find(selected);
		team = teamDAO.findTeamOf(penaltyShot.getPlayer());
		teamName = team.getName();
		player = penaltyShot.getPlayer().getId();
		return "";
	}

	public String update() {
		penaltyShot.setPlayer(playerDAO.find(player));
		penaltyShotDAO.update(penaltyShot);
		return "/index";
	}

	public String delete() {
		GamePeriodDAO gamePeriodDAO = new GamePeriodDAO(DataSource.createEntityManager());
		GamePeriod gp = gamePeriodDAO.findPeriodOf(penaltyShot);
		if (gp != null) {
			List<Event> loe = gp.getEvents();
			for (int i = 0; i < loe.size(); i++) {
				if (loe.get(i).getId() == penaltyShot.getId()) {
					loe.remove(i);
					break;
				}
			}
			gp.setEvents(new ArrayList<>(loe));
			gamePeriodDAO.update(gp);
		} else {
			OvertimeDAO overtimeDAO = new OvertimeDAO(DataSource.createEntityManager());
			Overtime o = overtimeDAO.findOvertimeOf(penaltyShot);
			if (o != null) {
				List<Event> loe = o.getEvents();
				for (int i = 0; i < loe.size(); i++) {
					if (loe.get(i).getId() == penaltyShot.getId()) {
						loe.remove(i);
						break;
					}
				}
				o.setEvents(new ArrayList<>(loe));
				overtimeDAO.update(o);
			}
		}
		penaltyShotDAO.delete(penaltyShot);
		return "/index";
	}

	public void teamChanged() {
		if (teamName == null || teamName.isEmpty()) {
			team = new Team();
		} else {
			team = teamDAO.find(teamName);
		}
	}

	public PenaltyShot getPenaltyShot() {
		return penaltyShot;
	}

	public void setPenaltyShot(PenaltyShot penaltyShot) {
		this.penaltyShot = penaltyShot;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<PenaltyShot> getPenaltyShots() {
		return penaltyShots;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setPenaltyShots(List<PenaltyShot> penaltyShots) {
		this.penaltyShots = penaltyShots;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public StartPosition[] getPositions() {
		return positions;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setPositions(StartPosition[] positions) {
		this.positions = positions;
	}

	public long getSelected() {
		return selected;
	}

	public void setSelected(long selected) {
		this.selected = selected;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public long getPlayer() {
		return player;
	}

	public void setPlayer(long player) {
		this.player = player;
	}

	public boolean isEnableEditing() {
		return enableEditing;
	}

	public void setEnableEditing(boolean enableEditing) {
		this.enableEditing = enableEditing;
	}
}
