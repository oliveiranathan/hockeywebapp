/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.Arena;
import model.Coach;
import model.Player;
import model.Team;
import model.dao.ArenaDAO;
import model.dao.CoachDAO;
import model.dao.DataSource;
import model.dao.PlayerDAO;
import model.dao.TeamDAO;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@ManagedBean
@ViewScoped
public class TeamBean {

	private final TeamDAO teamDAO = new TeamDAO(DataSource.createEntityManager());
	private final PlayerDAO playerDAO = new PlayerDAO(DataSource.createEntityManager());
	private final CoachDAO coachDAO = new CoachDAO(DataSource.createEntityManager());
	private final ArenaDAO arenaDAO = new ArenaDAO(DataSource.createEntityManager());
	private Team team = new Team();
	private List<Team> teams = teamDAO.findAll();
	private String selected = new String();
	private long coach;
	private String arena = new String();
	private List<String> selectedPlayers = new ArrayList<>(50);
	private String color = "FF0000";
	private boolean enableEditing;
	private boolean enableKey;

	public String insert() {
		Coach c = coachDAO.find(coach);
		if (c != null && c.getTeam() != null) {
			Team t = c.getTeam();
			t.setCoach(null);
			teamDAO.update(t);
			c.setTeam(null);
			coachDAO.update(c);
		}
		team.setCoach(c);
		Arena a = arenaDAO.find(arena);
		Team tA = teamDAO.findTeamOf(a);
		if (tA != null) {
			tA.setHome(null);
			teamDAO.update(tA);
		}
		team.setHome(a);
		List<Player> ps = new ArrayList<>(selectedPlayers.size());
		for (String s : selectedPlayers) {
			Player p = playerDAO.find(Long.parseLong(s));
			Team t = teamDAO.findTeamOf(p);
			if (t != null) {
				List<Player> lop = t.getPlayers();
				for (int i = 0; i < lop.size(); i++) {
					if (lop.get(i).getId() == p.getId()) {
						lop.remove(i);
						break;
					}
				}
				t.setPlayers(new ArrayList<>(lop));
				teamDAO.update(t);
			}
			ps.add(p);
		}
		team.setPlayers(ps);
		teamDAO.create(team);
		if (c != null) {
			c.setTeam(team);
			coachDAO.update(c);
		}
		return "/index";
	}

	public String find() {
		team = teamDAO.find(selected);
		coach = team.getCoach().getId();
		arena = team.getHome().getName();
		selectedPlayers = new ArrayList<>(team.getPlayers().size());
		for (Player p : team.getPlayers()) {
			selectedPlayers.add(Long.toString(p.getId()));
		}
		return "";
	}

	public String update() {
		Coach c = team.getCoach();
		if (c != null && !c.getTeam().equals(team)) {
			Team t = c.getTeam();
			t.setCoach(null);
			teamDAO.update(t);
			c.setTeam(team);
			coachDAO.update(c);
		}
		Arena a = arenaDAO.find(arena);
		Team tA = teamDAO.findTeamOf(a);
		if (tA != null && !tA.equals(team)) {
			tA.setHome(null);
			teamDAO.update(tA);
		}
		team.setHome(a);
		List<Player> ps = new ArrayList<>(selectedPlayers.size());
		for (String s : selectedPlayers) {
			Player p = playerDAO.find(Long.parseLong(s));
			Team t = teamDAO.findTeamOf(p);
			if (t != null) {
				List<Player> lop = t.getPlayers();
				for (int i = 0; i < lop.size(); i++) {
					if (lop.get(i).getId() == p.getId()) {
						lop.remove(i);
						break;
					}
				}
				t.setPlayers(new ArrayList<>(lop));
				teamDAO.update(t);
			}
			ps.add(p);
		}
		team.setPlayers(ps);
		teamDAO.update(team);
		return "/index";
	}

	public String delete() {
		if (team.getCoach() != null) {
			Coach c = team.getCoach();
			c.setTeam(null);
			coachDAO.update(c);
		}
		teamDAO.delete(team);
		return "/index";
	}

	public void addColor() {
		Color c = Color.decode("#" + color);
		team.getColors().add(c);
	}

	public String playerName(long pId) {
		return playerDAO.find(pId).getName();
	}

	public String getStyleFor(String color) {
		Pattern p = Pattern.compile("java.awt.Color\\[r=([0-9]+),g=([0-9]+),b=([0-9]+)\\]");
		Matcher m = p.matcher(color);
		if (m.matches()) {
			Color c = new Color(Integer.valueOf(m.group(1)),
					Integer.valueOf(m.group(2)),
					Integer.valueOf(m.group(3)));
			return String.format("#%02X%02X%02X", c.getRed(), c.getGreen(), c.getBlue());
		}
		return "#FFFFFF";
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<Team> getTeams() {
		return teams;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setTeams(List<Team> teams) {
		this.teams = teams;
	}

	public String getSelected() {
		return selected;
	}

	public void setSelected(String selected) {
		this.selected = selected;
	}

	public long getCoach() {
		return coach;
	}

	public void setCoach(long coach) {
		this.coach = coach;
	}

	public String getArena() {
		return arena;
	}

	public void setArena(String arena) {
		this.arena = arena;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<String> getSelectedPlayers() {
		return selectedPlayers;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setSelectedPlayers(List<String> selectedPlayers) {
		this.selectedPlayers = selectedPlayers;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public boolean isEnableEditing() {
		return enableEditing;
	}

	public void setEnableEditing(boolean enableEditing) {
		this.enableEditing = enableEditing;
	}

	public boolean isEnableKey() {
		return enableKey;
	}

	public void setEnableKey(boolean enableKey) {
		this.enableKey = enableKey;
	}
}
