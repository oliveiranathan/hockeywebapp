/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.Player;
import model.Position;
import model.Team;
import model.dao.DataSource;
import model.dao.PlayerDAO;
import model.dao.TeamDAO;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@ManagedBean
@ViewScoped
public class PlayerBean {

	private final PlayerDAO playerDAO = new PlayerDAO(DataSource.createEntityManager());
	private final TeamDAO teamDAO = new TeamDAO(DataSource.createEntityManager());
	private Player player = new Player();
	private List<Player> players = playerDAO.findAll();
	private Position[] positions = Position.values();
	private long selected = -1;
	private boolean enableEditing;

	public String insert() {
		playerDAO.create(player);
		return "/index";
	}

	public String update() {
		playerDAO.update(player);
		return "/index";
	}

	public String find() {
		player = playerDAO.find(selected);
		return "";
	}

	public String delete() {
		Team t = teamDAO.findTeamOf(player);
		if (t != null) {
			List<Player> lop = t.getPlayers();
			for (int i = 0; i < lop.size(); i++) {
				if (lop.get(i).getId() == player.getId()) {
					lop.remove(i);
					break;
				}
			}
			t.setPlayers(new ArrayList<>(lop));
			teamDAO.update(t);
		}
		playerDAO.delete(player);
		return "/index";
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<Player> getPlayers() {
		return players;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public Position[] getPositions() {
		return positions;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setPositions(Position[] positions) {
		this.positions = positions;
	}

	public long getSelected() {
		return selected;
	}

	public void setSelected(long selected) {
		this.selected = selected;
	}

	public boolean isEnableEditing() {
		return enableEditing;
	}

	public void setEnableEditing(boolean enableEditing) {
		this.enableEditing = enableEditing;
	}
}
