/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.Event;
import model.GamePeriod;
import model.Overtime;
import model.Penalty;
import model.PenaltyType;
import model.Team;
import model.dao.DataSource;
import model.dao.GamePeriodDAO;
import model.dao.OvertimeDAO;
import model.dao.PenaltyDAO;
import model.dao.PlayerDAO;
import model.dao.TeamDAO;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@ManagedBean
@ViewScoped
public class PenaltyBean {

	private final PenaltyDAO penaltyDAO = new PenaltyDAO(DataSource.createEntityManager());
	private final TeamDAO teamDAO = new TeamDAO(DataSource.createEntityManager());
	private final PlayerDAO playerDAO = new PlayerDAO(DataSource.createEntityManager());
	private Penalty penalty = new Penalty();
	private List<Penalty> penalties = penaltyDAO.findAll();
	private long selected = -1;
	private PenaltyType[] types = PenaltyType.values();
	private String offenderTeamName = new String();
	private String offendedTeamName = new String();
	private Team offenderTeam = new Team();
	private Team offendedTeam = new Team();
	private long offender = -1;
	private long served = -1;
	private long offended = -1;
	private boolean enableEditing;

	public String insert() {
		penalty.setOffended(playerDAO.find(offended));
		penalty.setOffender(playerDAO.find(offender));
		penalty.setServed(playerDAO.find(served));
		penaltyDAO.create(penalty);
		return "/index";
	}

	public String find() {
		penalty = penaltyDAO.find(selected);
		offenderTeam = teamDAO.findTeamOf(penalty.getOffender());
		offendedTeam = teamDAO.findTeamOf(penalty.getOffended());
		offendedTeamName = offendedTeam.getName();
		offenderTeamName = offenderTeam.getName();
		offender = penalty.getOffender().getId();
		offended = penalty.getOffended().getId();
		served = penalty.getServed().getId();
		return "";
	}

	public String update() {
		penalty.setOffended(playerDAO.find(offended));
		penalty.setOffender(playerDAO.find(offender));
		penalty.setServed(playerDAO.find(served));
		penaltyDAO.update(penalty);
		return "/index";
	}

	public String delete() {
		GamePeriodDAO gamePeriodDAO = new GamePeriodDAO(DataSource.createEntityManager());
		GamePeriod gp = gamePeriodDAO.findPeriodOf(penalty);
		if (gp != null) {
			List<Event> loe = gp.getEvents();
			for (int i = 0; i < loe.size(); i++) {
				if (loe.get(i).getId() == penalty.getId()) {
					loe.remove(i);
					break;
				}
			}
			gp.setEvents(new ArrayList<>(loe));
			gamePeriodDAO.update(gp);
		} else {
			OvertimeDAO overtimeDAO = new OvertimeDAO(DataSource.createEntityManager());
			Overtime o = overtimeDAO.findOvertimeOf(penalty);
			if (o != null) {
				List<Event> loe = o.getEvents();
				for (int i = 0; i < loe.size(); i++) {
					if (loe.get(i).getId() == penalty.getId()) {
						loe.remove(i);
						break;
					}
				}
				o.setEvents(new ArrayList<>(loe));
				overtimeDAO.update(o);
			}
		}
		penaltyDAO.delete(penalty);
		return "/index";
	}

	public void offenderTeamChanged() {
		if (offenderTeamName == null || offenderTeamName.isEmpty()) {
			offenderTeam = new Team();
		} else {
			offenderTeam = teamDAO.find(offenderTeamName);
		}
	}

	public void offendedTeamChanged() {
		if (offendedTeamName == null || offendedTeamName.isEmpty()) {
			offendedTeam = new Team();
		} else {
			offendedTeam = teamDAO.find(offendedTeamName);
		}
	}

	public long getSelected() {
		return selected;
	}

	public void setSelected(long selected) {
		this.selected = selected;
	}

	public Penalty getPenalty() {
		return penalty;
	}

	public void setPenalty(Penalty penalty) {
		this.penalty = penalty;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<Penalty> getPenalties() {
		return penalties;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setPenalties(List<Penalty> penalties) {
		this.penalties = penalties;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public PenaltyType[] getTypes() {
		return types;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setTypes(PenaltyType[] types) {
		this.types = types;
	}

	public Team getOffendedTeam() {
		return offendedTeam;
	}

	public void setOffendedTeam(Team offendedTeam) {
		this.offendedTeam = offendedTeam;
	}

	public String getOffendedTeamName() {
		return offendedTeamName;
	}

	public void setOffendedTeamName(String offendedTeamName) {
		this.offendedTeamName = offendedTeamName;
	}

	public Team getOffenderTeam() {
		return offenderTeam;
	}

	public void setOffenderTeam(Team offenderTeam) {
		this.offenderTeam = offenderTeam;
	}

	public String getOffenderTeamName() {
		return offenderTeamName;
	}

	public void setOffenderTeamName(String offenderTeamName) {
		this.offenderTeamName = offenderTeamName;
	}

	public long getOffender() {
		return offender;
	}

	public void setOffender(long offender) {
		this.offender = offender;
	}

	public long getServed() {
		return served;
	}

	public void setServed(long served) {
		this.served = served;
	}

	public long getOffended() {
		return offended;
	}

	public void setOffended(long offended) {
		this.offended = offended;
	}

	public boolean isEnableEditing() {
		return enableEditing;
	}

	public void setEnableEditing(boolean enableEditing) {
		this.enableEditing = enableEditing;
	}
}
