/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.Event;
import model.Faceoff;
import model.GamePeriod;
import model.Overtime;
import model.Player;
import model.StartPosition;
import model.Team;
import model.dao.DataSource;
import model.dao.FaceoffDAO;
import model.dao.GamePeriodDAO;
import model.dao.OvertimeDAO;
import model.dao.PlayerDAO;
import model.dao.TeamDAO;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@ManagedBean
@ViewScoped
public class FaceoffBean {

	private final FaceoffDAO faceoffDAO = new FaceoffDAO(DataSource.createEntityManager());
	private final TeamDAO teamDAO = new TeamDAO(DataSource.createEntityManager());
	private final PlayerDAO playerDAO = new PlayerDAO(DataSource.createEntityManager());
	private Faceoff faceoff = new Faceoff();
	private List<Faceoff> faceoffs = faceoffDAO.findAll();
	private long selected = -1;
	private String teamNameOne = new String();
	private String teamNameTwo = new String();
	private Team teamOne = new Team();
	private Team teamTwo = new Team();
	private long p1 = -1;
	private long p2 = -1;
	private StartPosition[] positions = StartPosition.values();
	private boolean enableEditing;

	public String insert() {
		faceoff.getPlayers().add(playerDAO.find(p1));
		faceoff.getPlayers().add(playerDAO.find(p2));
		faceoffDAO.create(faceoff);
		return "/index";
	}

	public String find() {
		faceoff = faceoffDAO.find(selected);
		teamOne = teamDAO.findTeamOf(faceoff.getPlayers().get(0));
		teamTwo = teamDAO.findTeamOf(faceoff.getPlayers().get(1));
		teamNameOne = teamOne.getName();
		teamNameTwo = teamTwo.getName();
		p1 = faceoff.getPlayers().get(0).getId();
		p2 = faceoff.getPlayers().get(1).getId();
		return "";
	}

	public String update() {
		List<Player> players = new ArrayList<>(2);
		players.add(playerDAO.find(p1));
		players.add(playerDAO.find(p2));
		faceoff.setPlayers(players);
		faceoffDAO.update(faceoff);
		return "/index";
	}

	public String delete() {
		GamePeriodDAO gamePeriodDAO = new GamePeriodDAO(DataSource.createEntityManager());
		GamePeriod gp = gamePeriodDAO.findPeriodOf(faceoff);
		if (gp != null) {
			List<Event> loe = gp.getEvents();
			for (int i = 0; i < loe.size(); i++) {
				if (loe.get(i).getId() == faceoff.getId()) {
					loe.remove(i);
					break;
				}
			}
			gp.setEvents(new ArrayList<>(loe));
			gamePeriodDAO.update(gp);
		} else {
			OvertimeDAO overtimeDAO = new OvertimeDAO(DataSource.createEntityManager());
			Overtime o = overtimeDAO.findOvertimeOf(faceoff);
			if (o != null) {
				List<Event> loe = o.getEvents();
				for (int i = 0; i < loe.size(); i++) {
					if (loe.get(i).getId() == faceoff.getId()) {
						loe.remove(i);
						break;
					}
				}
				o.setEvents(new ArrayList<>(loe));
				overtimeDAO.update(o);
			}
		}
		faceoffDAO.delete(faceoff);
		return "/index";
	}

	public void teamOneChanged() {
		if (teamNameOne == null || teamNameOne.isEmpty()) {
			teamOne = new Team();
		} else {
			teamOne = teamDAO.find(teamNameOne);
		}
	}

	public void teamTwoChanged() {
		if (teamNameTwo == null || teamNameTwo.isEmpty()) {
			teamTwo = new Team();
		} else {
			teamTwo = teamDAO.find(teamNameTwo);
		}
	}

	public Faceoff getFaceoff() {
		return faceoff;
	}

	public void setFaceoff(Faceoff faceoff) {
		this.faceoff = faceoff;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<Faceoff> getFaceoffs() {
		return faceoffs;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setFaceoffs(List<Faceoff> faceoffs) {
		this.faceoffs = faceoffs;
	}

	public long getSelected() {
		return selected;
	}

	public void setSelected(long selected) {
		this.selected = selected;
	}

	public String getTeamNameOne() {
		return teamNameOne;
	}

	public void setTeamNameOne(String teamNameOne) {
		this.teamNameOne = teamNameOne;
	}

	public String getTeamNameTwo() {
		return teamNameTwo;
	}

	public void setTeamNameTwo(String teamNameTwo) {
		this.teamNameTwo = teamNameTwo;
	}

	public Team getTeamOne() {
		return teamOne;
	}

	public void setTeamOne(Team teamOne) {
		this.teamOne = teamOne;
	}

	public Team getTeamTwo() {
		return teamTwo;
	}

	public void setTeamTwo(Team teamTwo) {
		this.teamTwo = teamTwo;
	}

	public long getP1() {
		return p1;
	}

	public void setP1(long p1) {
		this.p1 = p1;
	}

	public long getP2() {
		return p2;
	}

	public void setP2(long p2) {
		this.p2 = p2;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public StartPosition[] getPositions() {
		return positions;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setPositions(StartPosition[] positions) {
		this.positions = positions;
	}

	public boolean isEnableEditing() {
		return enableEditing;
	}

	public void setEnableEditing(boolean enableEditing) {
		this.enableEditing = enableEditing;
	}
}
