/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.bean;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.League;
import model.Official;
import model.OfficialPosition;
import model.dao.DataSource;
import model.dao.LeagueDAO;
import model.dao.OfficialDAO;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@ManagedBean
@ViewScoped
public class OfficialBean {

	private final OfficialDAO officialDAO = new OfficialDAO(DataSource.createEntityManager());
	private final LeagueDAO leagueDAO = new LeagueDAO(DataSource.createEntityManager());
	private Official official = new Official();
	private List<Official> officials = officialDAO.findAll();
	private long selected = -1;
	private OfficialPosition[] positions = OfficialPosition.values();
	private List<String> selectedLeagues = new ArrayList<>(1);
	private boolean enableEditing;

	public String insert() {
		official.setLeagues(getLeagues(selectedLeagues));
		officialDAO.create(official);
		return "/index";
	}

	public String update() {
		official.setLeagues(getLeagues(selectedLeagues));
		officialDAO.update(official);
		return "/index";
	}

	private List<League> getLeagues(List<String> names) {
		List<League> list = new ArrayList<>(names.size());
		for (String name : names) {
			list.add(leagueDAO.find(name));
		}
		return list;
	}

	public String find() {
		official = officialDAO.find(selected);
		selectedLeagues = new ArrayList<>(official.getLeagues().size());
		for (League l : official.getLeagues()) {
			selectedLeagues.add(l.getName());
		}
		return "";
	}

	public String delete() {
		for (League l : official.getLeagues()) {
			List<Official> os = new ArrayList<>(l.getOfficials());
			for (int i = 0; i < os.size(); i++) {
				if (os.get(i).getId() == official.getId()) {
					os.remove(i);
					break;
				}
			}
			l.setOfficials(os);
			leagueDAO.update(l);
		}
		officialDAO.delete(official);
		return "/index";
	}

	public Official getOfficial() {
		return official;
	}

	public void setOfficial(Official official) {
		this.official = official;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<Official> getOfficials() {
		return officials;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setOfficials(List<Official> officials) {
		this.officials = officials;
	}

	public long getSelected() {
		return selected;
	}

	public void setSelected(long selected) {
		this.selected = selected;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public OfficialPosition[] getPositions() {
		return positions;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setPositions(OfficialPosition[] positions) {
		this.positions = positions;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<String> getSelectedLeagues() {
		return selectedLeagues;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setSelectedLeagues(List<String> selectedLeagues) {
		this.selectedLeagues = selectedLeagues;
	}

	public boolean isEnableEditing() {
		return enableEditing;
	}

	public void setEnableEditing(boolean enableEditing) {
		this.enableEditing = enableEditing;
	}
}
