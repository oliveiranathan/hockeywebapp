package model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@Entity
public class Penalty extends Event implements Serializable {

	private static final long serialVersionUID = 1L;

	@Enumerated(EnumType.STRING)
	private PenaltyType type;
	@OneToOne
	private Player offender;
	@OneToOne
	private Player offended;
	@OneToOne
	private Player served;

	public PenaltyType getType() {
		return type;
	}

	public void setType(PenaltyType type) {
		this.type = type;
	}

	public Player getOffender() {
		return offender;
	}

	public void setOffender(Player offender) {
		this.offender = offender;
	}

	public Player getOffended() {
		return offended;
	}

	public void setOffended(Player offended) {
		this.offended = offended;
	}

	public Player getServed() {
		return served;
	}

	public void setServed(Player served) {
		this.served = served;
	}

	@Override
	public String toString() {
		return super.toString() + ": Penalty: " + offender.getName() + " " + type.getValue() + " " + offended.getName()
				+ (served.equals(offender) ? "" : ", " + served.getName() + " serving time.");
	}
}
