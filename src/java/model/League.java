package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Temporal;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@Entity
public class League implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private String name;
	@Temporal(javax.persistence.TemporalType.DATE)
	private Calendar since = new GregorianCalendar();
	private String abrangence;
	@ManyToMany
	private List<Team> teams = new ArrayList<>(10);
	@OneToMany
	@OrderBy("startDate")
	private List<Season> seasons = new ArrayList<>(10);
	@ManyToMany
	private List<Official> officials = new ArrayList<>(10);

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@SuppressWarnings("ReturnOfDateField")
	public Calendar getSince() {
		return since;
	}

	@SuppressWarnings("AssignmentToDateFieldFromParameter")
	public void setSince(Calendar since) {
		this.since = since;
	}

	public String getAbrangence() {
		return abrangence;
	}

	public void setAbrangence(String abrangence) {
		this.abrangence = abrangence;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<Team> getTeams() {
		return teams;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setTeams(List<Team> teams) {
		this.teams = teams;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<Season> getSeasons() {
		return seasons;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setSeasons(List<Season> seasons) {
		this.seasons = seasons;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<Official> getOfficials() {
		return officials;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setOfficials(List<Official> officials) {
		this.officials = officials;
	}
}
