package model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Temporal;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@Entity
public class Game implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private long id;
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Calendar startTime = new GregorianCalendar();
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Calendar endTime = new GregorianCalendar();
	@OneToMany
	@OrderBy("startTime")
	private List<GamePeriod> periods = new ArrayList<>(10);
	@OneToOne
	private Team homeTeam;
	@OneToOne
	private Team opposingTeam;
	@OneToMany
	private List<Official> officials = new ArrayList<>(10);

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@SuppressWarnings("ReturnOfDateField")
	public Calendar getStartTime() {
		return startTime;
	}

	@SuppressWarnings("AssignmentToDateFieldFromParameter")
	public void setStartTime(Calendar startTime) {
		this.startTime = startTime;
	}

	@SuppressWarnings("ReturnOfDateField")
	public Calendar getEndTime() {
		return endTime;
	}

	@SuppressWarnings("AssignmentToDateFieldFromParameter")
	public void setEndTime(Calendar endTime) {
		this.endTime = endTime;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<GamePeriod> getPeriods() {
		return periods;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setPeriods(List<GamePeriod> periods) {
		this.periods = periods;
	}

	public Team getHomeTeam() {
		return homeTeam;
	}

	public void setHomeTeam(Team homeTeam) {
		this.homeTeam = homeTeam;
	}

	public Team getOpposingTeam() {
		return opposingTeam;
	}

	public void setOpposingTeam(Team opposingTeam) {
		this.opposingTeam = opposingTeam;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<Official> getOfficials() {
		return officials;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setOfficials(List<Official> officials) {
		this.officials = officials;
	}

	@Override
	public String toString() {
		return homeTeam.getName() + " - " + opposingTeam.getName() + ": " + (new SimpleDateFormat("yyyy-MM-dd HH:mm").format(startTime.getTime()));
	}
}
