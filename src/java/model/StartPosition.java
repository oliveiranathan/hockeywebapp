package model;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public enum StartPosition {

	middleCircle("Middle Circle"),
	circleNW("NW Circle"),
	circleNE("NE Circle"),
	circleSE("SE Circle"),
	circleSW("SW Circle"),
	dotNW("NW Dot"),
	dotNE("NE Dot"),
	dotSE("SE Dot"),
	dotSW("SW Dot");

	private String value;

	private StartPosition(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
