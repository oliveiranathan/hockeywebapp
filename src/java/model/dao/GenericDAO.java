package model.dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 * @param <E> the class that holds the data
 * @param <K> the type of the key
 */
public abstract class GenericDAO<E, K> {

	protected final EntityManager em;
	@SuppressWarnings("unchecked")
	private final Class<E> clazz = (Class<E>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];

	public GenericDAO(EntityManager em) {
		this.em = em;
	}

	public void create(E entity) {
		em.getTransaction().begin();
		em.persist(entity);
		em.getTransaction().commit();
	}

	public void update(E entity) {
		em.getTransaction().begin();
		em.merge(entity);
		em.getTransaction().commit();
	}

	public void delete(E entity) {
		em.getTransaction().begin();
		em.remove(entity);
		em.getTransaction().commit();
	}

	public E find(K id) {
		try {
			return em.find(clazz, id);
		} catch (IllegalArgumentException ex) {
			return null;
		}
	}

	@SuppressWarnings({"unchecked", "JPQLValidation"})
	public List<E> findAll() {
		return em.createQuery("from " + clazz.getSimpleName()).getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<E> findPage(int max, int first) {
		@SuppressWarnings("JPQLValidation")
		Query query = em.createQuery("from " + clazz.getSimpleName());
		query.setMaxResults(max);
		query.setFirstResult(first);
		return query.getResultList();
	}

	@SuppressWarnings("JPQLValidation")
	public long getCount() {
		return (long) em.createQuery("select count(*) from " + clazz.getSimpleName()).getSingleResult();
	}
}
