package model.dao;

import javax.persistence.EntityManager;
import model.Goal;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public class GoalDAO extends GenericDAO<Goal, Long> {

	public GoalDAO(EntityManager em) {
		super(em);
	}

}
