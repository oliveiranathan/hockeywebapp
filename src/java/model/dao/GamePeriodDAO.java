package model.dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import model.Event;
import model.GamePeriod;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public class GamePeriodDAO extends GenericDAO<GamePeriod, Long> {

	public GamePeriodDAO(EntityManager em) {
		super(em);
	}

	public GamePeriod findPeriodOf(Event event) {
		Query query = em.createQuery("SELECT p FROM GamePeriod AS p WHERE :event MEMBER OF p.events");
		query.setParameter("event", event);
		try {
			return (GamePeriod) query.getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
	}
}
