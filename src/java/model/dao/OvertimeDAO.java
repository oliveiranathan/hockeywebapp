package model.dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import model.Event;
import model.Overtime;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public class OvertimeDAO extends GenericDAO<Overtime, Long> {

	public OvertimeDAO(EntityManager em) {
		super(em);
	}

	public Overtime findOvertimeOf(Event event) {
		Query query = em.createQuery("SELECT o FROM Overtime AS o WHERE :event MEMBER OF o.events");
		query.setParameter("event", event);
		try {
			return (Overtime) query.getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
	}
}
