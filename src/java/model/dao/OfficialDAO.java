package model.dao;

import javax.persistence.EntityManager;
import model.Official;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public class OfficialDAO extends GenericDAO<Official, Long> {

	public OfficialDAO(EntityManager em) {
		super(em);
	}

}
