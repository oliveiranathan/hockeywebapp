package model.dao;

import javax.persistence.EntityManager;
import model.Stoppage;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public class StoppageDAO extends GenericDAO<Stoppage, Long> {

	public StoppageDAO(EntityManager em) {
		super(em);
	}

}
