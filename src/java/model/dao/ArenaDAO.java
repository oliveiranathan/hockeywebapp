package model.dao;

import javax.persistence.EntityManager;
import model.Arena;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public class ArenaDAO extends GenericDAO<Arena, String> {

	public ArenaDAO(EntityManager em) {
		super(em);
	}

}
