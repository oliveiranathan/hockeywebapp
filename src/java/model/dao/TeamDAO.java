package model.dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import model.Arena;
import model.Player;
import model.Team;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public class TeamDAO extends GenericDAO<Team, String> {

	public TeamDAO(EntityManager em) {
		super(em);
	}

	public Team findTeamOf(Player player) {
		Query query = em.createQuery("SELECT t FROM Team AS t WHERE :player MEMBER OF t.players");
		query.setParameter("player", player);
		try {
			return (Team) query.getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
	}

	public Team findTeamOf(Arena arena) {
		Query query = em.createQuery("SELECT t FROM Team AS t WHERE t.home = :arena");
		query.setParameter("arena", arena);
		try {
			return (Team) query.getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
	}
}
