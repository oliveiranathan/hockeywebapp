package model.dao;

import javax.persistence.EntityManager;
import model.PenaltyShot;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public class PenaltyShotDAO extends GenericDAO<PenaltyShot, Long> {

	public PenaltyShotDAO(EntityManager em) {
		super(em);
	}

}
