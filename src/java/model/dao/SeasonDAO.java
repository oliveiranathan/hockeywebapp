package model.dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import model.Game;
import model.Season;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public class SeasonDAO extends GenericDAO<Season, Long> {

	public SeasonDAO(EntityManager em) {
		super(em);
	}

	public Season findSeasonOf(Game game) {
		Query query = em.createQuery("SELECT s FROM Season AS s WHERE :game MEMBER OF s.games");
		query.setParameter("game", game);
		try {
			return (Season) query.getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
	}
}
