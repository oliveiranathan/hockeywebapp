package model.dao;

import javax.persistence.EntityManager;
import model.Coach;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public class CoachDAO extends GenericDAO<Coach, Long> {

	public CoachDAO(EntityManager em) {
		super(em);
	}

}
