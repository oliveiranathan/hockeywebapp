package model.dao;

import javax.persistence.EntityManager;
import model.Event;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public class EventDAO extends GenericDAO<Event, Long> {

	public EventDAO(EntityManager em) {
		super(em);
	}

}
