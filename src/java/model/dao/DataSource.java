package model.dao;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import model.Arena;
import model.Coach;
import model.Faceoff;
import model.Game;
import model.GamePeriod;
import model.Goal;
import model.League;
import model.Official;
import model.OfficialPosition;
import model.Penalty;
import model.PenaltyShot;
import model.PenaltyType;
import model.Player;
import model.Position;
import model.Reason;
import model.Season;
import model.StartPosition;
import model.Stoppage;
import model.Substitution;
import model.Team;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public class DataSource {

	// <editor-fold defaultstate="collapsed" desc="Plain text data for the parsers">
	private static final String officialsData = "Official(_, Jacob Brenk		    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Francis Charron	    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Tom Chmielewski	    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Gord Dwyer		    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Eric Furlatt	    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Trevor Hanson	    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Mike Hasenfratz	    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Ghislain Hebert	    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Jean Hebert		    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Dave Jackson	    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Marc Joannette	    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Greg Kimmerly	    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Tom Kowal		    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Steve Kozari	    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Dennis LaRue	    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Frederick L'Ecuyer  , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Chris Lee		    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Mike Leggo		    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Mark Lemelin	    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Dave Lewis		    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, TJ Luxmore		    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Rob Martell		    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Wes McCauley	    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Jon McIsaac		    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Brad Meier		    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Dean Morton		    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Kendrick Nicholson  , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Dan O'Halloran	    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Dan O'Rourke	    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Tim Peel		    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Brian Pochmara	    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Kevin Pollock	    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Garrett Rank	    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Kyle Rehman		    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Evgeny Romasko	    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Chris Rooney	    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Graham Skilliter	, 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Francois St. Laurent, 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Justin St. Pierre	, 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Kelly Sutherland	, 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Ian Walsh		    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Brad Watson		    , 30,  referee	    )[{National Hockey League}]\n"
			+ "Official(_, Shandor Alphonso	, 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Derek Amell		    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Steve Barton	    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Devin Berg		    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, David Brisebois	    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Lonnie Cameron	    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Pierre Champoux	    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Scott Cherrey	    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Michel Cormier	    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Mike Cvik		    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Greg Devorski	    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Scott Driscoll	    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Ryan Galloway	    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Brandon Gawryletz	, 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Ryan Gibbons	    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Darren Gibbs	    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, John Grandt		    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Don Henderson	    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Shane Heyer		    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Trent Knorr		    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Brad Kovachik	    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Brad Lazarowich	    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Brian Mach		    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Matt MacPherson	    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Andy McElman	    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Steve Miller	    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Kiel Murchison	    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Brian Murphy	    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Jonny Murray	    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Derek Nansen	    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Thor Nelson		    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Tim Nowak		    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Bryan Pancich	    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Pierre Racicot	    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Vaughan Rody	    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Anthony Sericolo	, 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Jay Sharrers	    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Mark Shewchyk	    , 30,  linesman	    )[{National Hockey League}]\n"
			+ "Official(_, Mark Wheler		    , 30,  linesman	    )[{National Hockey League}]";
	private static final String arenasData = "Arena(Honda Center				, 60.96, 25.9)\n"
			+ "Arena(Gila River Arena			, 60.96, 25.9)\n"
			+ "Arena(TD Garden					, 60.96, 25.9)\n"
			+ "Arena(First Niagara Center		, 60.96, 25.9)\n"
			+ "Arena(Scotiabank Saddledome		, 60.96, 25.9)\n"
			+ "Arena(PNC Arena					, 60.96, 25.9)\n"
			+ "Arena(United Center				, 60.96, 25.9)\n"
			+ "Arena(Pepsi Center				, 60.96, 25.9)\n"
			+ "Arena(Nationwide Arena			, 60.96, 25.9)\n"
			+ "Arena(American Airlines Center	, 60.96, 25.9)\n"
			+ "Arena(Joe Louis Arena			, 60.96, 25.9)\n"
			+ "Arena(Rexall Place				, 60.96, 25.9)\n"
			+ "Arena(BB&T Center			, 60.96, 25.9)\n"
			+ "Arena(Staples Center			, 60.96, 25.9)\n"
			+ "Arena(Xcel Energy Center		, 60.96, 25.9)\n"
			+ "Arena(Bell Centre				, 60.96, 25.9)\n"
			+ "Arena(Bridgestone Arena			, 60.96, 25.9)\n"
			+ "Arena(Prudential Center			, 60.96, 25.9)\n"
			+ "Arena(Barclays Center			, 60.96, 25.9)\n"
			+ "Arena(Madison Square Garden		, 60.96, 25.9)\n"
			+ "Arena(Canadian Tire Centre		, 60.96, 25.9)\n"
			+ "Arena(Wells Fargo Center		, 60.96, 25.9)\n"
			+ "Arena(Consol Energy Center		, 60.96, 25.9)\n"
			+ "Arena(SAP Center at San Jose	, 60.96, 25.9)\n"
			+ "Arena(Scottrade Center			, 60.96, 25.9)\n"
			+ "Arena(Amalie Arena				, 60.96, 25.9)\n"
			+ "Arena(Air Canada Centre			, 60.96, 25.9)\n"
			+ "Arena(Rogers Arena				, 60.96, 25.9)\n"
			+ "Arena(Verizon Center			, 60.96, 25.9)\n"
			+ "Arena(MTS Centre				, 60.96, 25.9)";
	private static final String playerData = "ADP {\n"
			+ "Player(_, Frederik Andersen , 26, G , 31)\n"
			+ "Player(_, Kevin Bieksa	    , 34, D , 2 )\n"
			+ "Player(_, Andrew Cogliano   , 28, C , 7 )\n"
			+ "Player(_, Simon Despres	    , 24, D , 24)\n"
			+ "Player(_, Cam Fowler	    , 23, D , 4 )\n"
			+ "Player(_, Ryan Getzlaf	    , 30, C , 15)\n"
			+ "Player(_, Carl Hagelin	    , 27, LW, 26)\n"
			+ "Player(_, Kenton Helgesen   , 21, LW, 57)\n"
			+ "Player(_, Korbinian Holzer  , 27, D , 5 )\n"
			+ "Player(_, Shawn Horcoff	    , 37, C , 22)\n"
			+ "Player(_, Tim Jackman	    , 33, RW, 18)\n"
			+ "Player(_, Ryan Kesler	    , 31, C , 17)\n"
			+ "Player(_, Anton Khudobin    , 29, G , 30)\n"
			+ "Player(_, Hampus Lindholm   , 21, D , 47)\n"
			+ "Player(_, Josh Manson	    , 24, D , 42)\n"
			+ "Player(_, Patrick Maroon    , 27, LW, 19)\n"
			+ "Player(_, Corey Perry	    , 30, RW, 10)\n"
			+ "Player(_, Joe Piskula	    , 31, D , 49)\n"
			+ "Player(_, Rickard Rakell    , 22, RW, 67)\n"
			+ "Player(_, Mike Santorelli   , 29, C , 25)\n"
			+ "Player(_, Jiri Sekac	    , 23, LW, 46)\n"
			+ "Player(_, Jakob Silfverberg , 25, RW, 33)\n"
			+ "Player(_, Chris Stewart	    , 27, RW, 29)\n"
			+ "Player(_, Clayton Stoner    , 30, D , 3 )\n"
			+ "Player(_, Nate Thompson	    , 31, C , 44)\n"
			+ "Player(_, Sami Vatanen	    , 24, D , 45)\n"
			+ "}\n"
			+ "\n"
			+ "ACP {\n"
			+ "Player(_, Mikkel Boedker    	, 25, LW, 89)\n"
			+ "Player(_, Kyle Chipchura    	, 29, C , 24)\n"
			+ "Player(_, Klas Dahlbeck	    	, 24, D , 34)\n"
			+ "Player(_, Shane Doan			, 39, RW, 19)\n"
			+ "Player(_, Max Domi				, 20, LW, 16)\n"
			+ "Player(_, Steve Downie	    	, 28, RW, 17)\n"
			+ "Player(_, Anthony Duclair   	, 20, LW, 10)\n"
			+ "Player(_, Oliver Ekman-Larsson	, 24, D , 23)\n"
			+ "Player(_, Stefan Elliott    	, 24, D , 45)\n"
			+ "Player(_, Tyler Gaudet	    	, 22, C , 32)\n"
			+ "Player(_, Boyd Gordon	    	, 32, C , 15)\n"
			+ "Player(_, Nicklas Grossmann 	, 30, D , 2 )\n"
			+ "Player(_, Martin Hanzal	    	, 28, C , 11)\n"
			+ "Player(_, Anders Lindback   	, 27, G , 29)\n"
			+ "Player(_, Jordan Martinook  	, 23, LW, 48)\n"
			+ "Player(_, Zbynek Michalek   	, 32, D , 4 )\n"
			+ "Player(_, Connor Murphy	    	, 22, D , 5 )\n"
			+ "Player(_, Chris Pronger	    	, 41, D , 66)\n"
			+ "Player(_, Dylan Reese	    	, 31, D , 46)\n"
			+ "Player(_, Brad Richardson		, 30, RW, 12)\n"
			+ "Player(_, Tobias Rieder			, 22, RW, 8 )\n"
			+ "Player(_, John Scott			, 33, LW, 28)\n"
			+ "Player(_, Mike Smith			, 33, G , 41)\n"
			+ "Player(_, Michael Stone			, 25, D , 26)\n"
			+ "Player(_, Antoine Vermette		, 33, C , 50)\n"
			+ "Player(_, Joe Vitale			, 30, C , 14)\n"
			+ "}\n"
			+ "\n"
			+ "BBP {\n"
			+ "Player(_, Matt Beleskey	    , 27, LW, 39)\n"
			+ "Player(_, Patrice Bergeron  , 30, C , 37)\n"
			+ "Player(_, Zdeno Chara	    , 38, D , 33)\n"
			+ "Player(_, Brett Connolly    , 23, RW, 14)\n"
			+ "Player(_, Tommy Cross	    , 26, D , 56)\n"
			+ "Player(_, Loui Eriksson	    , 30, RW, 21)\n"
			+ "Player(_, Seth Griffith	    , 22, RW, 53)\n"
			+ "Player(_, Jonas Gustavsson  , 31, G , 50)\n"
			+ "Player(_, Jimmy Hayes	    , 25, RW, 11)\n"
			+ "Player(_, Chris Kelly	    , 34, LW, 23)\n"
			+ "Player(_, Joonas Kemppainen , 27, C , 41)\n"
			+ "Player(_, David Krejci	    , 29, C , 46)\n"
			+ "Player(_, Torey Krug	    , 24, D , 47)\n"
			+ "Player(_, Brad Marchand	    , 27, LW, 63)\n"
			+ "Player(_, Adam McQuaid	    , 29, D , 54)\n"
			+ "Player(_, Colin Miller	    , 23, D , 48)\n"
			+ "Player(_, Kevan Miller	    , 27, D , 86)\n"
			+ "Player(_, Joseph Morrow	    , 22, D , 45)\n"
			+ "Player(_, David Pastrnak    , 19, RW, 88)\n"
			+ "Player(_, Tyler Randell	    , 24, RW, 64)\n"
			+ "Player(_, Tuukka Rask	    , 28, G , 40)\n"
			+ "Player(_, Zac Rinaldo	    , 25, LW, 36)\n"
			+ "Player(_, Dennis Seidenberg , 34, D , 44)\n"
			+ "Player(_, Ryan Spooner	    , 23, C , 51)\n"
			+ "Player(_, Maxime Talbot	    , 31, C , 25)\n"
			+ "Player(_, Zach Trotman	    , 25, D , 62)\n"
			+ "}\n"
			+ "\n"
			+ "BSP {\n"
			+ "Player(_, Zach Bogosian			, 25, D , 47)\n"
			+ "Player(_, Carlo Colaiacovo		, 32, D , 25)\n"
			+ "Player(_, Nicolas Deslauriers	, 24, LW, 44)\n"
			+ "Player(_, Jack Eichel			, 18, C , 15)\n"
			+ "Player(_, Tyler Ennis			, 26, LW, 63)\n"
			+ "Player(_, Marcus Foligno		, 24, LW, 82)\n"
			+ "Player(_, Cody Franson			, 28, D , 46)\n"
			+ "Player(_, Brian Gionta			, 36, RW, 12)\n"
			+ "Player(_, Zemgus Girgensons		, 21, C , 28)\n"
			+ "Player(_, Josh Gorges			, 31, D , 4 )\n"
			+ "Player(_, Chad Johnson			, 29, G , 31)\n"
			+ "Player(_, Evander Kane			, 24, LW, 9 )\n"
			+ "Player(_, Johan Larsson			, 23, LW, 22)\n"
			+ "Player(_, David Legwand			, 35, C , 17)\n"
			+ "Player(_, Robin Lehner			, 24, G , 40)\n"
			+ "Player(_, Jake McCabe			, 22, D , 29)\n"
			+ "Player(_, Cody McCormick		, 32, RW, 8 )\n"
			+ "Player(_, Jamie McGinn			, 27, LW, 88)\n"
			+ "Player(_, Matt Moulson			, 31, LW, 26)\n"
			+ "Player(_, Mark Pysyk			, 23, D , 3 )\n"
			+ "Player(_, Ryan O'Reilly			, 24, C , 90)\n"
			+ "Player(_, Rasmus Ristolainen	, 21, D , 55)\n"
			+ "Player(_, Sam Reinhart			, 19, C , 23)\n"
			+ "Player(_, Bobby Sanguinetti		, 27, D , 24)\n"
			+ "Player(_, Tim Schaller			, 24, LW, 59)\n"
			+ "Player(_, Linus Ullmark			, 22, G , 35)\n"
			+ "Player(_, Mike Weber			, 27, D , 6 )\n"
			+ "}\n"
			+ "\n"
			+ "CFP {\n"
			+ "Player(_, Mikael Backlund	, 26, C , 11)\n"
			+ "Player(_, Sam Bennett		, 19, C , 93)\n"
			+ "Player(_, Brandon Bollig	, 28, LW, 52)\n"
			+ "Player(_, Lance Bouma		, 25, C , 17)\n"
			+ "Player(_, T. J. Brodie		, 25, D , 7 )\n"
			+ "Player(_, Joe Colborne		, 25, C , 8 )\n"
			+ "Player(_, Deryk Engelland	, 33, D , 29)\n"
			+ "Player(_, Michael Ferland	, 23, LW, 79)\n"
			+ "Player(_, Michael Frolik	, 27, W , 67)\n"
			+ "Player(_, Johnny Gaudreau	, 22, LW, 13)\n"
			+ "Player(_, Mark Giordano		, 32, D , 5 )\n"
			+ "Player(_, Dougie Hamilton	, 22, D , 27)\n"
			+ "Player(_, Jonas Hiller		, 33, G , 1 )\n"
			+ "Player(_, Jiri Hudler		, 31, RW, 24)\n"
			+ "Player(_, David Jones		, 31, RW, 19)\n"
			+ "Player(_, Josh Jooris		, 25, C , 16)\n"
			+ "Player(_, Sean Monahan		, 21, C , 23)\n"
			+ "Player(_, Jakub Nakladal	, 27, D , 49)\n"
			+ "Player(_, Joni Ortio		, 24, G , 37)\n"
			+ "Player(_, Mason Raymond		, 30, LW, 21)\n"
			+ "Player(_, Kris Russell		, 28, D , 4 )\n"
			+ "Player(_, Ladislav Smid		, 29, D , 15)\n"
			+ "Player(_, Matt Stajan		, 31, C , 18)\n"
			+ "Player(_, Dennis Wideman	, 32, D , 6 )\n"
			+ "}\n"
			+ "\n"
			+ "CHP {\n"
			+ "Player(_, Justin Faulk		, 23, D , 27)\n"
			+ "Player(_, Nathan Gerbe		, 28, LW, 14)\n"
			+ "Player(_, Ron Hainsey		, 34, D , 65)\n"
			+ "Player(_, Noah Hanifin		, 18, D , 5 )\n"
			+ "Player(_, Michal Jordan		, 25, D , 47)\n"
			+ "Player(_, Eddie Lack		, 27, G , 31)\n"
			+ "Player(_, John-Michael Liles, 34, D , 26)\n"
			+ "Player(_, Elias Lindholm	, 20, C , 16)\n"
			+ "Player(_, Brad Malone		, 26, C , 24)\n"
			+ "Player(_, Jay McClement		, 32, C , 18)\n"
			+ "Player(_, Brock McGinn		, 21, LW, 23)\n"
			+ "Player(_, Ryan Murphy		, 22, D , 7 )\n"
			+ "Player(_, Riley Nash		, 26, C , 20)\n"
			+ "Player(_, Andrej Nestrasil	, 24, C , 15)\n"
			+ "Player(_, Joakim Nordstrom	, 23, C , 42)\n"
			+ "Player(_, Brett Pesce		, 20, D , 54)\n"
			+ "Player(_, Victor Rask		, 22, C , 49)\n"
			+ "Player(_, Jeff Skinner		, 23, LW, 53)\n"
			+ "Player(_, Eric Staal		, 30, C , 12)\n"
			+ "Player(_, Jordan Staal		, 27, C , 11)\n"
			+ "Player(_, Chris Terry		, 26, LW, 25)\n"
			+ "Player(_, Kris Versteeg		, 29, RW, 32)\n"
			+ "Player(_, Cam Ward			, 31, G , 30)\n"
			+ "Player(_, James Wisniewski	, 31, D , 21)\n"
			+ "}\n"
			+ "\n"
			+ "CBP {\n"
			+ "Player(_, Artem Anisimov		, 27, C , 15)\n"
			+ "Player(_, Bryan Bickell			, 29, LW, 29)\n"
			+ "Player(_, Corey Crawford		, 30, G , 50)\n"
			+ "Player(_, Trevor Daley			, 32, D , 6 )\n"
			+ "Player(_, Scott Darling			, 26, G , 33)\n"
			+ "Player(_, Andrew Desjardins		, 29, C , 11)\n"
			+ "Player(_, Ryan Garbutt			, 30, C , 28)\n"
			+ "Player(_, Erik Gustafsson		, 23, D , 52)\n"
			+ "Player(_, Niklas Hjalmarsson	, 28, D , 4 )\n"
			+ "Player(_, Marian Hossa			, 36, RW, 81)\n"
			+ "Player(_, Patrick Kane			, 26, RW, 88)\n"
			+ "Player(_, Duncan Keith			, 32, D , 2 )\n"
			+ "Player(_, Tanner Kero			, 23, C , 67)\n"
			+ "Player(_, Marcus Kruger			, 25, C , 16)\n"
			+ "Player(_, Artemi Panarin		, 23, F , 72)\n"
			+ "Player(_, Michal Rozsival		, 37, D , 32)\n"
			+ "Player(_, David Rundblad		, 25, D , 5 )\n"
			+ "Player(_, Brent Seabrook		, 30, D , 7 )\n"
			+ "Player(_, Andrew Shaw			, 24, C , 65)\n"
			+ "Player(_, Viktor Svedberg		, 24, D , 43)\n"
			+ "Player(_, Teuvo Teravainen		, 21, C , 86)\n"
			+ "Player(_, Viktor Tikhonov		, 27, RW, 14)\n"
			+ "Player(_, Jonathan Toews		, 27, C , 19)\n"
			+ "Player(_, Trevor van Riemsdyk	, 24, D , 57)\n"
			+ "}\n"
			+ "\n"
			+ "CAP {\n"
			+ "Player(_, Tyson Barrie			, 24, D , 4 )\n"
			+ "Player(_, Francois Beauchemin	, 35, D , 32)\n"
			+ "Player(_, Reto Berra			, 28, G , 20)\n"
			+ "Player(_, Blake Comeau			, 29, LW, 14)\n"
			+ "Player(_, Matt Duchene			, 24, C , 9 )\n"
			+ "Player(_, Dennis Everberg		, 23, LW, 45)\n"
			+ "Player(_, Brandon Gormley		, 23, D , 46)\n"
			+ "Player(_, Mikhail Grigorenko	, 21, C , 25)\n"
			+ "Player(_, Nate Guenin			, 32, D , 5 )\n"
			+ "Player(_, Nick Holden			, 28, D , 2 )\n"
			+ "Player(_, Jarome Iginla			, 38, RW, 12)\n"
			+ "Player(_, Erik Johnson			, 27, D , 6 )\n"
			+ "Player(_, Gabriel Landeskog		, 22, LW, 92)\n"
			+ "Player(_, Nathan MacKinnon		, 20, C , 29)\n"
			+ "Player(_, Cody McLeod			, 31, LW, 55)\n"
			+ "Player(_, John Mitchell			, 30, C , 7 )\n"
			+ "Player(_, Jack Skille			, 28, RW, 8 )\n"
			+ "Player(_, Carl Soderberg		, 30, C , 34)\n"
			+ "Player(_, Brad Stuart			, 35, D , 17)\n"
			+ "Player(_, Alex Tanguay			, 35, RW, 40)\n"
			+ "Player(_, Semyon Varlamov		, 27, G , 1 )\n"
			+ "Player(_, Jesse Winchester		, 32, C , 18)\n"
			+ "Player(_, Nikita Zadorov		, 20, D , 16)\n"
			+ "}\n"
			+ "\n"
			+ "CJP {\n"
			+ "Player(_, Josh Anderson		, 21, RW, 34)\n"
			+ "Player(_, Cam Atkinson		, 26, RW, 13)\n"
			+ "Player(_, Sergei Bobrovsky	, 27, G , 72)\n"
			+ "Player(_, Jared Boll		, 29, RW, 40)\n"
			+ "Player(_, Rene Bourque		, 33, RW, 18)\n"
			+ "Player(_, Matt Calvert		, 25, LW, 11)\n"
			+ "Player(_, Gregory Campbell	, 31, C , 9 )\n"
			+ "Player(_, David Clarkson	, 31, RW, 23)\n"
			+ "Player(_, Kevin Connauton	, 25, D , 4 )\n"
			+ "Player(_, Brandon Dubinsky	, 29, LW, 17)\n"
			+ "Player(_, Nick Foligno		, 27, LW, 71)\n"
			+ "Player(_, Cody Goloubef		, 25, D , 29)\n"
			+ "Player(_, Scott Hartnell	, 33, LW, 43)\n"
			+ "Player(_, Boone Jenner		, 22, C , 38)\n"
			+ "Player(_, Ryan Johansen		, 23, C , 19)\n"
			+ "Player(_, Jack Johnson		, 28, D , 7 )\n"
			+ "Player(_, William Karlsson	, 22, C , 25)\n"
			+ "Player(_, Curtis McElhinney	, 32, G , 30)\n"
			+ "Player(_, Ryan Murray		, 22, D , 27)\n"
			+ "Player(_, Dalton Prout		, 25, D , 47)\n"
			+ "Player(_, Brandon Saad		, 22, LW, 20)\n"
			+ "Player(_, David Savard		, 25, D , 58)\n"
			+ "Player(_, Fedor Tyutin		, 32, D , 51)\n"
			+ "Player(_, Alexander Wennberg, 21, C , 41)\n"
			+ "}\n"
			+ "\n"
			+ "DSP {\n"
			+ "Player(_, Jamie Benn		, 26, LW, 14)\n"
			+ "Player(_, Jordie Benn		, 28, D , 24)\n"
			+ "Player(_, Jason Demers		, 27, D , 4 )\n"
			+ "Player(_, Cody Eakin		, 24, C , 20)\n"
			+ "Player(_, Patrick Eaves		, 31, C , 18)\n"
			+ "Player(_, Radek Faksa		, 21, C , 12)\n"
			+ "Player(_, Vernon Fiddler	, 35, C , 38)\n"
			+ "Player(_, Alex Goligoski	, 30, D , 33)\n"
			+ "Player(_, Ales Hemsky		, 32, RW, 83)\n"
			+ "Player(_, Mattias Janmark	, 22, C , 13)\n"
			+ "Player(_, Jyrki Jokipakka	, 24, D , 2 )\n"
			+ "Player(_, John Klingberg	, 23, D , 3 )\n"
			+ "Player(_, Kari Lehtonen		, 31, G , 32)\n"
			+ "Player(_, Curtis McKenzie	, 24, LW, 11)\n"
			+ "Player(_, Travis Moen		, 33, LW, 27)\n"
			+ "Player(_, Patrik Nemeth		, 23, D , 15)\n"
			+ "Player(_, Valeri Nichushkin	, 20, RW, 43)\n"
			+ "Player(_, Antti Niemi		, 32, G , 31)\n"
			+ "Player(_, Jamie Oleksiak	, 22, D , 5 )\n"
			+ "Player(_, Johnny Oduya		, 34, D , 47)\n"
			+ "Player(_, Brett Ritchie		, 22, RW, 25)\n"
			+ "Player(_, Antoine Roussel	, 25, LW, 21)\n"
			+ "Player(_, Colton Sceviour	, 26, RW, 22)\n"
			+ "Player(_, Tyler Seguin		, 23, RW, 91)\n"
			+ "Player(_, Patrick Sharp		, 33, LW, 10)\n"
			+ "Player(_, Jason Spezza		, 32, C , 90)\n"
			+ "}\n"
			+ "\n"
			+ "DRP {\n"
			+ "Player(_, Justin Abdelkader	, 28, LW, 8 )\n"
			+ "Player(_, Joakim Andersson	, 26, C , 18)\n"
			+ "Player(_, Pavel Datsyuk		, 37, C , 13)\n"
			+ "Player(_, Danny DeKeyser	, 25, D , 65)\n"
			+ "Player(_, Jonathan Ericsson	, 31, D , 52)\n"
			+ "Player(_, Landon Ferraro	, 24, C , 29)\n"
			+ "Player(_, Johan Franzen		, 35, RW, 93)\n"
			+ "Player(_, Luke Glendening	, 26, C , 41)\n"
			+ "Player(_, Mike Green		, 30, D , 25)\n"
			+ "Player(_, Darren Helm		, 28, C , 43)\n"
			+ "Player(_, Jimmy Howard		, 31, G , 35)\n"
			+ "Player(_, Tomas Jurco		, 22, RW, 26)\n"
			+ "Player(_, Jakub Kindl		, 28, D , 4 )\n"
			+ "Player(_, Niklas Kronwall	, 34, D , 55)\n"
			+ "Player(_, Dylan Larkin		, 19, C , 71)\n"
			+ "Player(_, Alexei Marchenko	, 23, D , 47)\n"
			+ "Player(_, Drew Miller		, 31, LW, 20)\n"
			+ "Player(_, Petr Mrazek		, 23, G , 34)\n"
			+ "Player(_, Gustav Nyquist	, 26, RW, 14)\n"
			+ "Player(_, Teemu Pulkkinen	, 23, RW, 56)\n"
			+ "Player(_, Kyle Quincey		, 30, D , 27)\n"
			+ "Player(_, Brad Richards		, 35, C , 17)\n"
			+ "Player(_, Riley Sheahan		, 23, C , 15)\n"
			+ "Player(_, Brendan Smith		, 26, D , 2 )\n"
			+ "Player(_, Tomas Tatar		, 24, LW, 21)\n"
			+ "Player(_, Henrik Zetterberg	, 35, C , 40)\n"
			+ "}\n"
			+ "\n"
			+ "EOP {\n"
			+ "Player(_, Brandon Davidson		, 24, D , 88)\n"
			+ "Player(_, Jordan Eberle			, 25, RW, 14)\n"
			+ "Player(_, Mark Fayne			, 28, D , 5 )\n"
			+ "Player(_, Andrew Ference		, 36, D , 21)\n"
			+ "Player(_, Luke Gazdic			, 26, LW, 20)\n"
			+ "Player(_, Eric Gryba			, 27, D , 62)\n"
			+ "Player(_, Taylor Hall			, 23, LW, 4 )\n"
			+ "Player(_, Matt Hendricks		, 34, LW, 23)\n"
			+ "Player(_, Oscar Klefbom			, 22, D , 77)\n"
			+ "Player(_, Rob Klinkhammer		, 29, LW, 12)\n"
			+ "Player(_, Lauri Korpikoski		, 29, LW, 28)\n"
			+ "Player(_, Anton Lander			, 24, C , 51)\n"
			+ "Player(_, Mark Letestu			, 30, C , 55)\n"
			+ "Player(_, Connor McDavid		, 18, C , 97)\n"
			+ "Player(_, Anders Nilsson		, 25, G , 39)\n"
			+ "Player(_, Ryan Nugent-Hopkins	, 22, C , 93)\n"
			+ "Player(_, Darnell Nurse			, 20, D , 25)\n"
			+ "Player(_, Iiro Pakarinen		, 24, RW, 26)\n"
			+ "Player(_, Benoit Pouliot		, 29, LW, 67)\n"
			+ "Player(_, Teddy Purcell			, 30, RW, 16)\n"
			+ "Player(_, Griffin Reinhart		, 21, D , 8 )\n"
			+ "Player(_, Justin Schultz		, 25, D , 19)\n"
			+ "Player(_, Andrej Sekera			, 29, D , 2 )\n"
			+ "Player(_, Anton Slepyshev		, 21, LW, 42)\n"
			+ "Player(_, Cam Talbot			, 28, G , 33)\n"
			+ "Player(_, Nail Yakupov			, 22, RW, 10)\n"
			+ "}\n"
			+ "\n"
			+ "FPP {\n"
			+ "Player(_, Aleksander Barkov	, 20, C , 16)\n"
			+ "Player(_, Nick Bjugstad		, 23, C , 27)\n"
			+ "Player(_, Dave Bolland		, 29, C , 63)\n"
			+ "Player(_, Connor Brickley	, 23, C , 86)\n"
			+ "Player(_, Brian Campbell	, 36, D , 51)\n"
			+ "Player(_, Aaron Ekblad		, 19, D , 5 )\n"
			+ "Player(_, Erik Gudbranson	, 23, D , 44)\n"
			+ "Player(_, Jonathan Huberdeau, 22, LW, 11)\n"
			+ "Player(_, Jaromir Jagr		, 43, RW, 68)\n"
			+ "Player(_, Jussi Jokinen		, 32, LW, 36)\n"
			+ "Player(_, Steven Kampfer	, 27, D , 3 )\n"
			+ "Player(_, Dmitry Kulikov	, 24, D , 7 )\n"
			+ "Player(_, Roberto Luongo	, 36, G , 1 )\n"
			+ "Player(_, Derek MacKenzie	, 34, C , 17)\n"
			+ "Player(_, Willie Mitchell	, 38, D , 33)\n"
			+ "Player(_, Al Montoya		, 30, G , 35)\n"
			+ "Player(_, Alex Petrovic		, 23, D , 6 )\n"
			+ "Player(_, Brandon Pirri		, 24, C , 73)\n"
			+ "Player(_, Marc Savard		, 38, C , 66)\n"
			+ "Player(_, Reilly Smith		, 24, RW, 18)\n"
			+ "Player(_, Shawn Thornton	, 38, LW, 22)\n"
			+ "Player(_, Vincent Trocheck	, 22, C , 21)\n"
			+ "}\n"
			+ "\n"
			+ "LAP {\n"
			+ "Player(_, Andy Andreoff		, 24, LW, 15)\n"
			+ "Player(_, Dustin Brown		, 30, RW, 23)\n"
			+ "Player(_, Jeff Carter		, 30, RW, 77)\n"
			+ "Player(_, Kyle Clifford		, 24, LW, 13)\n"
			+ "Player(_, Drew Doughty		, 25, D , 8 )\n"
			+ "Player(_, Christian Ehrhoff	, 33, D , 10)\n"
			+ "Player(_, Jhonas Enroth		, 27, G , 1 )\n"
			+ "Player(_, Derek Forbort		, 23, D , 7 )\n"
			+ "Player(_, Marian Gaborik	, 33, RW, 12)\n"
			+ "Player(_, Matt Greene		, 32, D , 2 )\n"
			+ "Player(_, Dwight King		, 26, LW, 74)\n"
			+ "Player(_, Anze Kopitar		, 28, C , 11)\n"
			+ "Player(_, Trevor Lewis		, 28, RW, 22)\n"
			+ "Player(_, Milan Lucic		, 27, LW, 17)\n"
			+ "Player(_, Alec Martinez		, 28, D , 27)\n"
			+ "Player(_, Brayden McNabb	, 24, D , 3 )\n"
			+ "Player(_, Jake Muzzin		, 26, D , 6 )\n"
			+ "Player(_, Jordan Nolan		, 26, RW, 71)\n"
			+ "Player(_, Tanner Pearson	, 23, LW, 70)\n"
			+ "Player(_, Jonathan Quick	, 29, G , 32)\n"
			+ "Player(_, Nick Shore		, 23, C , 37)\n"
			+ "Player(_, Tyler Toffoli		, 23, RW, 73)\n"
			+ "Player(_, Jordan Weal		, 23, C , 19)\n"
			+ "}\n"
			+ "\n"
			+ "MWP {\n"
			+ "Player(_, Niklas Backstrom	, 37, G , 32)\n"
			+ "Player(_, Jonas Brodin		, 22, D , 25)\n"
			+ "Player(_, Ryan Carter		, 32, C , 18)\n"
			+ "Player(_, Charlie Coyle		, 23, C , 3 )\n"
			+ "Player(_, Devan Dubnyk		, 29, G , 40)\n"
			+ "Player(_, Matt Dumba		, 21, D , 24)\n"
			+ "Player(_, Christian Folin	, 24, D , 5 )\n"
			+ "Player(_, Justin Fontaine	, 27, RW, 14)\n"
			+ "Player(_, Mikael Granlund	, 23, C , 64)\n"
			+ "Player(_, Tyler Graovac		, 22, C , 44)\n"
			+ "Player(_, Erik Haula		, 24, C , 56)\n"
			+ "Player(_, Mikko Koivu		, 32, C , 9 )\n"
			+ "Player(_, Darcy Kuemper		, 25, G , 35)\n"
			+ "Player(_, Nino Niederreiter	, 23, RW, 22)\n"
			+ "Player(_, Zach Parise		, 31, LW, 11)\n"
			+ "Player(_, Jason Pominville	, 32, RW, 29)\n"
			+ "Player(_, Chris Porter		, 31, LW, 7 )\n"
			+ "Player(_, Nate Prosser		, 29, D , 39)\n"
			+ "Player(_, Marco Scandella	, 25, D , 6 )\n"
			+ "Player(_, Jared Spurgeon	, 25, D , 46)\n"
			+ "Player(_, Ryan Suter		, 30, D , 20)\n"
			+ "Player(_, Thomas Vanek		, 31, LW, 26)\n"
			+ "Player(_, Jason Zucker		, 23, LW, 16)\n"
			+ "}\n"
			+ "\n"
			+ "MCP {\n"
			+ "Player(_, Nathan Beaulieu		, 22, D , 28)\n"
			+ "Player(_, Michael Bournival		, 23, LW, 49)\n"
			+ "Player(_, Paul Byron			, 26, LW, 41)\n"
			+ "Player(_, Mike Condon			, 25, G , 39)\n"
			+ "Player(_, David Desharnais		, 29, C , 51)\n"
			+ "Player(_, Lars Eller			, 26, LW, 81)\n"
			+ "Player(_, Alexei Emelin			, 29, D , 74)\n"
			+ "Player(_, Tomas Fleischmann		, 31, LW, 15)\n"
			+ "Player(_, Brian Flynn			, 27, LW, 32)\n"
			+ "Player(_, Alex Galchenyuk		, 21, C , 27)\n"
			+ "Player(_, Brendan Gallagher		, 23, RW, 11)\n"
			+ "Player(_, Tom Gilbert			, 32, D , 77)\n"
			+ "Player(_, Zack Kassian			, 24, RW, 8 )\n"
			+ "Player(_, Andrei Markov			, 36, D , 79)\n"
			+ "Player(_, Torrey Mitchell		, 30, C , 17)\n"
			+ "Player(_, Max Pacioretty		, 26, LW, 67)\n"
			+ "Player(_, Greg Pateryn			, 25, D , 6 )\n"
			+ "Player(_, Jeff Petry			, 27, D , 26)\n"
			+ "Player(_, Tomas Plekanec		, 32, C , 14)\n"
			+ "Player(_, Carey Price			, 28, G , 31)\n"
			+ "Player(_, Alexander Semin		, 31, RW, 13)\n"
			+ "Player(_, Devante Smith-Pelly	, 23, RW, 21)\n"
			+ "Player(_, P. K. Subban			, 26, D , 76)\n"
			+ "Player(_, Jarred Tinordi		, 23, D , 24)\n"
			+ "Player(_, Dale Weise			, 27, RW, 22)\n"
			+ "}\n"
			+ "\n"
			+ "NPP {\n"
			+ "Player(_, Viktor Arvidsson	, 22, LW, 38)\n"
			+ "Player(_, Victor Bartley	, 27, D , 64)\n"
			+ "Player(_, Anthony Bitetto	, 25, D , 2 )\n"
			+ "Player(_, Gabriel Bourque	, 25, RW, 57)\n"
			+ "Player(_, Mattias Ekholm	, 25, D , 14)\n"
			+ "Player(_, Ryan Ellis		, 24, D , 4 )\n"
			+ "Player(_, Mike Fisher		, 35, C , 12)\n"
			+ "Player(_, Filip Forsberg	, 21, LW, 9 )\n"
			+ "Player(_, Paul Gaustad		, 33, C , 28)\n"
			+ "Player(_, Cody Hodgson		, 25, C , 11)\n"
			+ "Player(_, Carter Hutton		, 29, G , 30)\n"
			+ "Player(_, Barret Jackman	, 34, D , 5 )\n"
			+ "Player(_, Calle Jarnkrok	, 24, C , 19)\n"
			+ "Player(_, Seth Jones		, 21, D , 3 )\n"
			+ "Player(_, Roman Josi		, 25, D , 59)\n"
			+ "Player(_, James Neal		, 28, RW, 18)\n"
			+ "Player(_, Eric Nystrom		, 32, LW, 24)\n"
			+ "Player(_, Mike Ribeiro		, 35, C , 63)\n"
			+ "Player(_, Pekka Rinne		, 32, G , 35)\n"
			+ "Player(_, Craig Smith		, 26, RW, 15)\n"
			+ "Player(_, Austin Watson		, 23, LW, 51)\n"
			+ "Player(_, Shea Weber		, 30, D , 6 )\n"
			+ "Player(_, Colin Wilson		, 26, LW, 33)\n"
			+ "}\n"
			+ "\n"
			+ "NJP {\n"
			+ "Player(_, Michael Cammalleri, 33, LW, 13)\n"
			+ "Player(_, Ryane Clowe		, 33, LW, 29)\n"
			+ "Player(_, Patrik Elias		, 39, LW, 26)\n"
			+ "Player(_, Bobby Farnham		, 26, LW, 23)\n"
			+ "Player(_, Eric Gelinas		, 24, D , 44)\n"
			+ "Player(_, Stephen Gionta	, 32, C , 11)\n"
			+ "Player(_, Andy Greene		, 32, D , 6 )\n"
			+ "Player(_, Adam Henrique		, 25, LW, 14)\n"
			+ "Player(_, Jacob Josefson	, 24, LW, 16)\n"
			+ "Player(_, Sergei Kalinin	, 24, RW, 51)\n"
			+ "Player(_, Keith Kinkaid		, 26, G , 1 )\n"
			+ "Player(_, Adam Larsson		, 22, D , 5 )\n"
			+ "Player(_, Stefan Matteau	, 21, LW, 25)\n"
			+ "Player(_, Jon Merrill		, 23, D , 7 )\n"
			+ "Player(_, John Moore		, 24, D , 2 )\n"
			+ "Player(_, Brian O'Neill		, 27, RW, 18)\n"
			+ "Player(_, Kyle Palmieri		, 24, RW, 21)\n"
			+ "Player(_, Tuomo Ruutu		, 32, LW, 15)\n"
			+ "Player(_, David Schlemko	, 28, D , 8 )\n"
			+ "Player(_, Cory Schneider	, 29, G , 35)\n"
			+ "Player(_, Damon Severson	, 21, D , 28)\n"
			+ "Player(_, Lee Stempniak		, 32, RW, 20)\n"
			+ "Player(_, Jiri Tlusty		, 27, LW, 9 )\n"
			+ "Player(_, Jordin Tootoo		, 32, RW, 22)\n"
			+ "Player(_, Travis Zajac		, 30, C , 19)\n"
			+ "}\n"
			+ "\n"
			+ "NIP {\n"
			+ "Player(_, Josh Bailey			, 26, C , 12)\n"
			+ "Player(_, Steve Bernier			, 30, RW, 16)\n"
			+ "Player(_, Jean-Francois Berube	, 24, G , 30)\n"
			+ "Player(_, Eric Boulton			, 39, LW, 36)\n"
			+ "Player(_, Johnny Boychuk		, 31, D , 55)\n"
			+ "Player(_, Casey Cizikas			, 24, C , 53)\n"
			+ "Player(_, Cal Clutterbuck		, 27, RW, 15)\n"
			+ "Player(_, Calvin de Haan		, 24, D , 44)\n"
			+ "Player(_, Mikhail Grabovski		, 31, C , 84)\n"
			+ "Player(_, Thomas Greiss			, 29, G , 1 )\n"
			+ "Player(_, Jaroslav Halak		, 30, G , 41)\n"
			+ "Player(_, Travis Hamonic		, 25, D , 3 )\n"
			+ "Player(_, Thomas Hickey			, 26, D , 14)\n"
			+ "Player(_, Nikolay Kulemin		, 29, LW, 86)\n"
			+ "Player(_, Nick Leddy			, 24, D , 2 )\n"
			+ "Player(_, Anders Lee			, 25, C , 27)\n"
			+ "Player(_, Matt Martin			, 26, LW, 17)\n"
			+ "Player(_, Brock Nelson			, 24, C , 29)\n"
			+ "Player(_, Frans Nielsen			, 31, C , 51)\n"
			+ "Player(_, Kyle Okposo			, 27, RW, 21)\n"
			+ "Player(_, Brian Strait			, 27, D , 37)\n"
			+ "Player(_, Ryan Strome			, 22, C , 18)\n"
			+ "Player(_, John Tavares			, 25, C , 91)\n"
			+ "Player(_, Marek Zidlicky		, 38, D , 28)\n"
			+ "}\n"
			+ "\n"
			+ "NRP {\n"
			+ "Player(_, Dan Boyle			, 39, D , 22)\n"
			+ "Player(_, Derick Brassard	, 28, C , 16)\n"
			+ "Player(_, Emerson Etem		, 23, RW, 96)\n"
			+ "Player(_, Jesper Fast		, 24, LW, 19)\n"
			+ "Player(_, Dan Girardi		, 31, D , 5 )\n"
			+ "Player(_, Tanner Glass		, 31, LW, 15)\n"
			+ "Player(_, Kevin Hayes		, 23, C , 13)\n"
			+ "Player(_, Kevin Klein		, 30, D , 8 )\n"
			+ "Player(_, Chris Kreider		, 24, LW, 20)\n"
			+ "Player(_, Oscar Lindberg	, 23, C , 24)\n"
			+ "Player(_, Henrik Lundqvist	, 33, G , 30)\n"
			+ "Player(_, Ryan McDonagh		, 26, D , 27)\n"
			+ "Player(_, Dylan McIlrath	, 23, D , 6 )\n"
			+ "Player(_, J. T. Miller		, 22, RW, 10)\n"
			+ "Player(_, Dominic Moore		, 35, C , 28)\n"
			+ "Player(_, Rick Nash			, 31, LW, 61)\n"
			+ "Player(_, Antti Raanta		, 26, G , 32)\n"
			+ "Player(_, Marc Staal		, 28, D , 18)\n"
			+ "Player(_, Viktor Stalberg	, 29, LW, 25)\n"
			+ "Player(_, Derek Stepan		, 25, C , 21)\n"
			+ "Player(_, Jarret Stoll		, 33, C , 26)\n"
			+ "Player(_, Keith Yandle		, 29, D , 93)\n"
			+ "Player(_, Mats Zuccarello	, 28, RW, 36)\n"
			+ "}\n"
			+ "\n"
			+ "OSP {\n"
			+ "Player(_, Craig Anderson		, 34, G , 41)\n"
			+ "Player(_, Mark Borowiecki		, 26, D , 74)\n"
			+ "Player(_, Cody Ceci				, 21, D , 5 )\n"
			+ "Player(_, Alex Chiasson			, 25, RW, 90)\n"
			+ "Player(_, Jared Cowen			, 24, D , 2 )\n"
			+ "Player(_, Andrew Hammond		, 27, G , 30)\n"
			+ "Player(_, Mike Hoffman			, 25, LW, 68)\n"
			+ "Player(_, Erik Karlsson			, 25, D , 65)\n"
			+ "Player(_, Curtis Lazar			, 20, C , 27)\n"
			+ "Player(_, Clarke MacArthur		, 30, LW, 16)\n"
			+ "Player(_, Max McCormick			, 23, LW, 89)\n"
			+ "Player(_, Marc Methot			, 30, D , 3 )\n"
			+ "Player(_, Milan Michalek		, 30, LW, 9 )\n"
			+ "Player(_, Chris Neil			, 36, RW, 25)\n"
			+ "Player(_, Jean-Gabriel Pageau	, 22, C , 44)\n"
			+ "Player(_, Chris Phillips		, 37, D , 4 )\n"
			+ "Player(_, Shane Prince			, 22, C , 10)\n"
			+ "Player(_, Matt Puempel			, 22, LW, 26)\n"
			+ "Player(_, Bobby Ryan			, 28, RW, 6 )\n"
			+ "Player(_, Zack Smith			, 27, C , 15)\n"
			+ "Player(_, Mark Stone			, 23, RW, 61)\n"
			+ "Player(_, Kyle Turris			, 26, C , 7 )\n"
			+ "Player(_, Chris Wideman			, 25, D , 45)\n"
			+ "Player(_, Patrick Wiercioch		, 25, D , 46)\n"
			+ "Player(_, Mika Zibanejad		, 22, C , 93)\n"
			+ "}\n"
			+ "\n"
			+ "PFP {\n"
			+ "Player(_, Pierre-Edouard Bellemare	, 30, LW, 78)\n"
			+ "Player(_, Sean Couturier			, 22, C , 14)\n"
			+ "Player(_, Michael Del Zotto			, 25, D , 15)\n"
			+ "Player(_, Sam Gagner				, 26, C , 89)\n"
			+ "Player(_, Claude Giroux				, 27, C , 28)\n"
			+ "Player(_, Radko Gudas				, 25, D , 3 )\n"
			+ "Player(_, Jason LaBarbera			, 35, G , 45)\n"
			+ "Player(_, Scott Laughton			, 21, C , 21)\n"
			+ "Player(_, Vincent Lecavalier		, 35, C , 40)\n"
			+ "Player(_, Brandon Manning			, 25, D , 23)\n"
			+ "Player(_, Steve Mason				, 27, G , 35)\n"
			+ "Player(_, Evgeny Medvedev			, 33, D , 82)\n"
			+ "Player(_, Michal Neuvirth			, 27, G , 30)\n"
			+ "Player(_, Michael Raffl				, 26, LW, 12)\n"
			+ "Player(_, Matt Read					, 29, RW, 24)\n"
			+ "Player(_, Brayden Schenn			, 24, C , 10)\n"
			+ "Player(_, Luke Schenn				, 25, D , 22)\n"
			+ "Player(_, Nick Schultz				, 33, D , 55)\n"
			+ "Player(_, Wayne Simmonds			, 27, RW, 17)\n"
			+ "Player(_, Mark Streit				, 37, D , 32)\n"
			+ "Player(_, R. J. Umberger			, 33, LW, 20)\n"
			+ "Player(_, Chris VandeVelde			, 28, C , 76)\n"
			+ "Player(_, Jakub Voracek				, 26, RW, 93)\n"
			+ "Player(_, Ryan White				, 27, C , 25)\n"
			+ "}\n"
			+ "\n"
			+ "PPP {\n"
			+ "Player(_, Beau Bennett		, 23, RW, 19)\n"
			+ "Player(_, Nick Bonino		, 27, C , 13)\n"
			+ "Player(_, Adam Clendening	, 23, D , 2 )\n"
			+ "Player(_, Ian Cole			, 26, D , 28)\n"
			+ "Player(_, Sidney Crosby		, 28, C , 87)\n"
			+ "Player(_, Matt Cullen		, 38, C , 7 )\n"
			+ "Player(_, Brian Dumoulin	, 24, D , 8 )\n"
			+ "Player(_, Pascal Dupuis		, 36, RW, 9 )\n"
			+ "Player(_, Eric Fehr			, 30, C , 16)\n"
			+ "Player(_, Marc-Andre Fleury	, 30, G , 29)\n"
			+ "Player(_, Patric Hornqvist	, 28, RW, 72)\n"
			+ "Player(_, Phil Kessel		, 28, RW, 81)\n"
			+ "Player(_, Chris Kunitz		, 36, LW, 14)\n"
			+ "Player(_, Kris Letang		, 28, D , 58)\n"
			+ "Player(_, Ben Lovejoy		, 31, D , 12)\n"
			+ "Player(_, Olli Maatta		, 21, D , 3 )\n"
			+ "Player(_, Evgeni Malkin		, 29, C , 71)\n"
			+ "Player(_, David Perron		, 27, LW, 57)\n"
			+ "Player(_, Sergei Plotnikov	, 25, LW, 61)\n"
			+ "Player(_, Kevin Porter		, 29, C , 11)\n"
			+ "Player(_, Bryan Rust		, 23, RW, 17)\n"
			+ "Player(_, Rob Scuderi		, 36, D , 4 )\n"
			+ "Player(_, Daniel Sprong		, 18, RW, 41)\n"
			+ "Player(_, Jeff Zatkoff		, 28, G , 37)\n"
			+ "}\n"
			+ "\n"
			+ "SJP {\n"
			+ "Player(_, Justin Braun			, 28, D , 61)\n"
			+ "Player(_, Mike Brown			, 30, RW, 18)\n"
			+ "Player(_, Brent Burns			, 30, D , 88)\n"
			+ "Player(_, Logan Couture			, 26, C , 39)\n"
			+ "Player(_, Brenden Dillon		, 24, D , 4 )\n"
			+ "Player(_, Joonas Donskoi		, 23, RW, 27)\n"
			+ "Player(_, Barclay Goodrow		, 22, RW, 89)\n"
			+ "Player(_, Micheal Haley			, 29, LW, 38)\n"
			+ "Player(_, Tomas Hertl			, 21, LW, 48)\n"
			+ "Player(_, Martin Jones			, 25, G , 31)\n"
			+ "Player(_, Bryan Lerg			, 29, LW, 11)\n"
			+ "Player(_, Patrick Marleau		, 36, LW, 12)\n"
			+ "Player(_, Paul Martin			, 34, D , 7 )\n"
			+ "Player(_, Mirco Mueller			, 20, D , 41)\n"
			+ "Player(_, Matthew Nieto			, 22, LW, 83)\n"
			+ "Player(_, Joe Pavelski			, 31, C , 8 )\n"
			+ "Player(_, Ben Smith				, 27, RW, 21)\n"
			+ "Player(_, Alex Stalock			, 28, G , 32)\n"
			+ "Player(_, Matt Tennyson			, 25, D , 80)\n"
			+ "Player(_, Joe Thornton			, 36, C , 19)\n"
			+ "Player(_, Chris Tierney			, 21, C , 50)\n"
			+ "Player(_, Marc-Edouard Vlasic	, 28, D , 44)\n"
			+ "Player(_, Joel Ward				, 34, RW, 42)\n"
			+ "Player(_, Tommy Wingels			, 27, RW, 57)\n"
			+ "}\n"
			+ "\n"
			+ "SLP {\n"
			+ "Player(_, Jake Allen		, 25, G , 34)\n"
			+ "Player(_, David Backes		, 31, RW, 42)\n"
			+ "Player(_, Patrik Berglund	, 27, C , 21)\n"
			+ "Player(_, Robert Bortuzzo	, 26, D , 41)\n"
			+ "Player(_, Jay Bouwmeester	, 32, D , 19)\n"
			+ "Player(_, Kyle Brodziak		, 31, C , 28)\n"
			+ "Player(_, Troy Brouwer		, 30, RW, 36)\n"
			+ "Player(_, Joel Edmundson	, 22, D , 6 )\n"
			+ "Player(_, Brian Elliott		, 30, G , 1 )\n"
			+ "Player(_, Robby Fabbri		, 19, C , 15)\n"
			+ "Player(_, Scott Gomez		, 35, C , 93)\n"
			+ "Player(_, Carl Gunnarsson	, 28, D , 4 )\n"
			+ "Player(_, Dmitrij Jaskin	, 22, RW, 23)\n"
			+ "Player(_, Jori Lehtera		, 27, C , 12)\n"
			+ "Player(_, Steve Ott			, 33, C , 9 )\n"
			+ "Player(_, Colton Parayko	, 22, D , 55)\n"
			+ "Player(_, Alex Pietrangelo	, 25, D , 27)\n"
			+ "Player(_, Ryan Reaves		, 28, RW, 75)\n"
			+ "Player(_, Jaden Schwartz	, 23, LW, 17)\n"
			+ "Player(_, Kevin Shattenkirk	, 26, D , 22)\n"
			+ "Player(_, Paul Stastny		, 29, C , 26)\n"
			+ "Player(_, Alexander Steen	, 31, LW, 20)\n"
			+ "Player(_, Vladimir Tarasenko, 23, RW, 91)\n"
			+ "Player(_, Scottie Upshall	, 32, RW, 10)\n"
			+ "}\n"
			+ "\n"
			+ "TBP {\n"
			+ "Player(_, Ben Bishop			, 28, G , 30)\n"
			+ "Player(_, Brian Boyle			, 30, C , 11)\n"
			+ "Player(_, J. T. Brown			, 25, RW, 23)\n"
			+ "Player(_, Ryan Callahan			, 30, RW, 24)\n"
			+ "Player(_, Matt Carle			, 31, D , 25)\n"
			+ "Player(_, Braydon Coburn		, 30, D , 55)\n"
			+ "Player(_, Erik Condra			, 29, RW, 22)\n"
			+ "Player(_, Jonathan Drouin		, 20, LW, 27)\n"
			+ "Player(_, Valtteri Filppula		, 31, C , 51)\n"
			+ "Player(_, Jason Garrison		, 30, D , 5 )\n"
			+ "Player(_, Kristers Gudlevskis	, 23, G , 37)\n"
			+ "Player(_, Victor Hedman			, 24, D , 77)\n"
			+ "Player(_, Tyler Johnson			, 25, C , 9 )\n"
			+ "Player(_, Alexander Killorn		, 26, C , 17)\n"
			+ "Player(_, Nikita Kucherov		, 22, RW, 86)\n"
			+ "Player(_, Jonathan Marchessault	, 24, LW, 42)\n"
			+ "Player(_, Vladislav Namestnikov	, 22, C , 90)\n"
			+ "Player(_, Nikita Nesterov		, 22, D , 89)\n"
			+ "Player(_, Ondrej Palat			, 24, LW, 18)\n"
			+ "Player(_, Cedric Paquette		, 22, C , 13)\n"
			+ "Player(_, Steven Stamkos		, 25, C , 91)\n"
			+ "Player(_, Anton Stralman		, 29, D , 6 )\n"
			+ "Player(_, Andrej Sustr			, 24, D , 62)\n"
			+ "Player(_, Andrei Vasilevskiy	, 21, G , 88)\n"
			+ "}\n"
			+ "\n"
			+ "TMP {\n"
			+ "Player(_, Mark Arcobello	, 27, C , 33)\n"
			+ "Player(_, Jonathan Bernier	, 27, G , 45)\n"
			+ "Player(_, Brad Boyes		, 33, RW, 28)\n"
			+ "Player(_, Tyler Bozak		, 29, C , 42)\n"
			+ "Player(_, Byron Froese		, 24, C , 56)\n"
			+ "Player(_, Jake Gardiner		, 25, D , 51)\n"
			+ "Player(_, Michael Grabner	, 28, RW, 40)\n"
			+ "Player(_, Scott Harrington	, 22, D , 36)\n"
			+ "Player(_, Peter Holland		, 24, C , 24)\n"
			+ "Player(_, Nathan Horton		, 30, RW, 66)\n"
			+ "Player(_, Matt Hunwick		, 30, D , 2 )\n"
			+ "Player(_, Nazem Kadri		, 25, C , 43)\n"
			+ "Player(_, Leo Komarov		, 28, C , 47)\n"
			+ "Player(_, Joffrey Lupul		, 32, LW, 19)\n"
			+ "Player(_, Martin Marincin	, 23, D , 52)\n"
			+ "Player(_, Shawn Matthias	, 27, C , 23)\n"
			+ "Player(_, P. A. Parenteau	, 32, RW, 15)\n"
			+ "Player(_, Dion Phaneuf		, 30, D , 3 )\n"
			+ "Player(_, Roman Polak		, 29, D , 46)\n"
			+ "Player(_, James Reimer		, 27, G , 34)\n"
			+ "Player(_, Morgan Rielly		, 21, D , 44)\n"
			+ "Player(_, Stephane Robidas	, 38, D , 12)\n"
			+ "Player(_, Nick Spaling		, 27, C , 16)\n"
			+ "Player(_, James van Riemsdyk, 26, LW, 21)\n"
			+ "Player(_, Daniel Winnik		, 30, LW, 26)\n"
			+ "}\n"
			+ "\n"
			+ "VCP {\n"
			+ "Player(_, Richard Bachman	, 28, G , 32)\n"
			+ "Player(_, Sven Baertschi	, 23, LW, 47)\n"
			+ "Player(_, Matt Bartkowski	, 27, D , 44)\n"
			+ "Player(_, Alexandre Burrows	, 34, RW, 14)\n"
			+ "Player(_, Adam Cracknell	, 30, RW, 24)\n"
			+ "Player(_, Derek Dorsett		, 28, RW, 15)\n"
			+ "Player(_, Alexander Edler	, 29, D , 23)\n"
			+ "Player(_, Dan Hamhuis		, 32, D , 2 )\n"
			+ "Player(_, Jannik Hansen		, 29, RW, 36)\n"
			+ "Player(_, Chris Higgins		, 32, LW, 20)\n"
			+ "Player(_, Bo Horvat			, 20, C , 53)\n"
			+ "Player(_, Ben Hutton		, 22, D , 27)\n"
			+ "Player(_, Jacob Markstrom	, 25, G , 25)\n"
			+ "Player(_, Jared McCann		, 19, C , 91)\n"
			+ "Player(_, Ryan Miller		, 35, G , 30)\n"
			+ "Player(_, Brandon Prust		, 31, LW, 9 )\n"
			+ "Player(_, Luca Sbisa		, 25, D , 5 )\n"
			+ "Player(_, Daniel Sedin		, 35, LW, 22)\n"
			+ "Player(_, Henrik Sedin		, 35, C , 33)\n"
			+ "Player(_, Brandon Sutter	, 26, C , 21)\n"
			+ "Player(_, Chris Tanev		, 25, D , 8 )\n"
			+ "Player(_, Jake Virtanen		, 19, RW, 18)\n"
			+ "Player(_, Radim Vrbata		, 34, RW, 17)\n"
			+ "Player(_, Yannick Weber		, 27, D , 6 )\n"
			+ "}\n"
			+ "\n"
			+ "WCP {\n"
			+ "Player(_, Karl Alzner			, 27, D , 27)\n"
			+ "Player(_, Nicklas Backstrom		, 27, C , 19)\n"
			+ "Player(_, Chris Brown			, 24, RW, 67)\n"
			+ "Player(_, Andre Burakovsky		, 20, LW, 65)\n"
			+ "Player(_, John Carlson			, 25, D , 74)\n"
			+ "Player(_, Jason Chimera			, 36, LW, 25)\n"
			+ "Player(_, Taylor Chorney		, 28, D , 4 )\n"
			+ "Player(_, Stanislav Galiev		, 23, RW, 49)\n"
			+ "Player(_, Philipp Grubauer		, 23, G , 31)\n"
			+ "Player(_, Braden Holtby			, 26, G , 70)\n"
			+ "Player(_, Marcus Johansson		, 25, LW, 90)\n"
			+ "Player(_, Evgeny Kuznetsov		, 23, F , 92)\n"
			+ "Player(_, Brooks Laich			, 32, C , 21)\n"
			+ "Player(_, Michael Latta			, 24, C , 46)\n"
			+ "Player(_, Matt Niskanen			, 28, D , 2 )\n"
			+ "Player(_, Dmitry Orlov			, 24, D , 9 )\n"
			+ "Player(_, Brooks Orpik			, 35, D , 44)\n"
			+ "Player(_, T. J. Oshie			, 28, RW, 77)\n"
			+ "Player(_, Alex Ovechkin			, 30, LW, 8 )\n"
			+ "Player(_, Nate Schmidt			, 24, D , 88)\n"
			+ "Player(_, Chandler Stephenson	, 21, LW, 18)\n"
			+ "Player(_, Justin Williams		, 34, RW, 14)\n"
			+ "Player(_, Tom Wilson			, 21, RW, 43)\n"
			+ "}\n"
			+ "\n"
			+ "WJP {\n"
			+ "Player(_, Alexander Burmistrov	, 24, C , 6 )\n"
			+ "Player(_, Dustin Byfuglien		, 30, RW, 33)\n"
			+ "Player(_, Ben Chiarot			, 25, D , 7 )\n"
			+ "Player(_, Andrew Copp			, 21, C , 9 )\n"
			+ "Player(_, Nikolaj Ehlers		, 19, LW, 27)\n"
			+ "Player(_, Tobias Enstrom		, 30, D , 39)\n"
			+ "Player(_, Michael Hutchinson	, 25, G , 34)\n"
			+ "Player(_, Andrew Ladd			, 29, LW, 16)\n"
			+ "Player(_, Bryan Little			, 27, C , 18)\n"
			+ "Player(_, Adam Lowry			, 22, C , 17)\n"
			+ "Player(_, Tyler Myers			, 25, D , 57)\n"
			+ "Player(_, Adam Pardy			, 31, D , 2 )\n"
			+ "Player(_, Ondrej Pavelec		, 28, G , 31)\n"
			+ "Player(_, Anthony Peluso		, 26, RW, 14)\n"
			+ "Player(_, Mathieu Perreault		, 27, LW, 85)\n"
			+ "Player(_, Nicolas Petan			, 20, C , 19)\n"
			+ "Player(_, Paul Postma			, 26, D , 4 )\n"
			+ "Player(_, Mark Scheifele		, 22, C , 55)\n"
			+ "Player(_, Drew Stafford			, 29, RW, 12)\n"
			+ "Player(_, Mark Stuart			, 31, D , 5 )\n"
			+ "Player(_, Chris Thorburn		, 32, RW, 22)\n"
			+ "Player(_, Jacob Trouba			, 21, D , 8 )\n"
			+ "Player(_, Blake Wheeler			, 29, RW, 26)\n"
			+ "}";
	private static final String coachsData = "Coach(_, Bruce Boudreau	    , 40)[Anaheim Ducks			]\n"
			+ "Coach(_, Dave Tippett	    , 40)[Arizona Coyotes		]   \n"
			+ "Coach(_, Claude Julien	    , 40)[Boston Bruins			]\n"
			+ "Coach(_, Dan Bylsma			, 40)[Buffalo Sabres		]   \n"
			+ "Coach(_, Bob Hartley	    , 40)[Calgary Flames		]   \n"
			+ "Coach(_, Bill Peters	    , 40)[Carolina Hurricanes   ]\n"
			+ "Coach(_, Joel Quenneville   , 40)[Chicago Blackhawks	]\n"
			+ "Coach(_, Patrick Roy	    , 40)[Colorado Avalanche	]   \n"
			+ "Coach(_, John Tortorella    , 40)[Columbus Blue Jackets	]\n"
			+ "Coach(_, Lindy Ruff			, 40)[Dallas Stars			]\n"
			+ "Coach(_, Jeff Blashill	    , 40)[Detroit Red Wings		]   \n"
			+ "Coach(_, Todd McLellan	    , 40)[Edmonton Oilers		]   \n"
			+ "Coach(_, Gerard Gallant	    , 40)[Florida Panthers		]   \n"
			+ "Coach(_, Darryl Sutter	    , 40)[Los Angeles Kings		]   \n"
			+ "Coach(_, Mike Yeo			, 40)[Minnesota Wild		]   \n"
			+ "Coach(_, Michel Therrien    , 40)[Montreal Canadiens	]   \n"
			+ "Coach(_, Peter Laviolette   , 40)[Nashville Predators   ]\n"
			+ "Coach(_, John Hynes			, 40)[New Jersey Devils		]   \n"
			+ "Coach(_, Jack Capuano	    , 40)[New York Islanders	]   \n"
			+ "Coach(_, Alain Vigneault    , 40)[New York Rangers		]   \n"
			+ "Coach(_, Dave Cameron	    , 40)[Ottawa Senators		]   \n"
			+ "Coach(_, Dave Hakstol	    , 40)[Philadelphia Flyers   ]\n"
			+ "Coach(_, Mike Johnston	    , 40)[Pittsburgh Penguins   ]\n"
			+ "Coach(_, Peter DeBoer	    , 40)[San Jose Sharks		]   \n"
			+ "Coach(_, Ken Hitchcock	    , 40)[St. Louis Blues		]   \n"
			+ "Coach(_, Jon Cooper			, 40)[Tampa Bay Lightning   ]\n"
			+ "Coach(_, Mike Babcock	    , 40)[Toronto Maple Leafs   ]\n"
			+ "Coach(_, Willie Desjardins  , 40)[Vancouver Canucks		]\n"
			+ "Coach(_, Barry Trotz	    , 40)[Washington Capitals	]\n"
			+ "Coach(_, Paul Maurice	    , 40)[Winnipeg Jets			]";
	private static final String teamsData = "TEAMS {\n"
			+ "Team(Anaheim Ducks			, 1993	, {#1E1E1E ;#B6985A ;#F57D31 ;#FFFFFF			})[Honda Center				, Bruce Boudreau	, ADP]\n"
			+ "Team(Arizona Coyotes	    , 1972	, {#98012E ;#EEE3C7 ;#000000 ;#FFFFFF			})[Gila River Arena			, Dave Tippett		, ACP]\n"
			+ "Team(Boston Bruins			, 1924	, {#000000 ;#FDB930 ;#FFFFFF					})[TD Garden				, Claude Julien		, BBP]\n"
			+ "Team(Buffalo Sabres			, 1970	, {#002D62 ;#FDB930 ;#A7A9AC ;#FFFFFF			})[First Niagara Center	    , Dan Bylsma		, BSP]\n"
			+ "Team(Calgary Flames			, 1972	, {#CB0D0D ;#FDBF12 ;#000000 ;#FFFFFF			})[Scotiabank Saddledome    , Bob Hartley		, CFP]\n"
			+ "Team(Carolina Hurricanes    , 1972	, {#E51A38 ;#000000 ;#FFFFFF					})[PNC Arena				, Bill Peters		, CHP]\n"
			+ "Team(Chicago Blackhawks	    , 1926	, {#C60C30 ;#000000 ;#FFFFFF					})[United Center			, Joel Quenneville	, CBP]\n"
			+ "Team(Colorado Avalanche	    , 1972	, {#822433 ;#165788 ;#85888B ;#000000			})[Pepsi Center				, Patrick Roy		, CAP]\n"
			+ "Team(Columbus Blue Jackets  , 1997-06-25, {#002147 ;#C60C30 ;#A5ACAF ;#FFFFFF		})[Nationwide Arena			, John Tortorella	, CJP]\n"
			+ "Team(Dallas Stars			, 1967	, {#016F4A ;#A7A8AC ;#000000 ;#FFFFFF			})[American Airlines Center	, Lindy Ruff		, DSP]\n"
			+ "Team(Detroit Red Wings	    , 1926	, {#E51837 ;#FFFFFF								})[Joe Louis Arena			, Jeff Blashill		, DRP]\n"
			+ "Team(Edmonton Oilers	    , 1972	, {#013E7F ;#EB6E1E ;#FFFFFF					})[Rexall Place				, Todd McLellan		, EOP]\n"
			+ "Team(Florida Panthers	    , 1993	, {#E51A38 ;#002D62 ;#D4A00F ;#FFFFFF			})[BB&amp;T Center			, Gerard Gallant	, FPP]\n"
			+ "Team(Los Angeles Kings	    , 1967	, {#000000 ;#B2B7BB ;#FFFFFF					})[Staples Center			, Darryl Sutter		, LAP]\n"
			+ "Team(Minnesota Wild			, 1997	, {#C51230 ;#004F30 ;#F1B310 ;#EEE3C7 ;#FFFFFF	})[Xcel Energy Center	    , Mike Yeo			, MWP]\n"
			+ "Team(Montreal Canadiens	    , 1909-12-04, {#C51230 ;#FFFFFF ;#083A81				})[Bell Centre				, Michel Therrien	, MCP]\n"
			+ "Team(Nashville Predators    , 1997-06-25, {#FDBB30 ;#002D62 ;#FFFFFF				})[Bridgestone Arena	    , Peter Laviolette	, NPP]\n"
			+ "Team(New Jersey Devils	    , 1974	, {#E51937 ;#000000 ;#FFFFFF					})[Prudential Center		, John Hynes		, NJP]\n"
			+ "Team(New York Islanders	    , 1972	, {#00529B ;#F57D31 ;#FFFFFF					})[Barclays Center			, Jack Capuano		, NIP]\n"
			+ "Team(New York Rangers	    , 1926	, {#0161AB ;#E6393F ;#FFFFFF					})[Madison Square Garden    , Alain Vigneault	, NRP]\n"
			+ "Team(Ottawa Senators	    , 1990	, {#E51837 ;#000000 ;#D4A00F ;#FFFFFF			})[Canadian Tire Centre	    , Dave Cameron		, OSP]\n"
			+ "Team(Philadelphia Flyers    , 1967	, {#F4793E ;#000000 ;#FFFFFF					})[Wells Fargo Center	    , Dave Hakstol		, PFP]\n"
			+ "Team(Pittsburgh Penguins    , 1967	, {#000000 ;#C5B358 ;#FFFFFF					})[Consol Energy Center	    , Mike Johnston		, PPP]\n"
			+ "Team(San Jose Sharks	    , 1991	, {#007889 ;#000000 ;#FFFFFF ;#F4901E			})[SAP Center at San Jose   , Peter DeBoer		, SJP]\n"
			+ "Team(St. Louis Blues	    , 1967	, {#00529C ;#FDB930 ;#002D62 ;#FFFFFF			})[Scottrade Center			, Ken Hitchcock		, SLP]\n"
			+ "Team(Tampa Bay Lightning    , 1992	, {#003D7C ;#FFFFFF ;#000000					})[Amalie Arena				, Jon Cooper		, TBP]\n"
			+ "Team(Toronto Maple Leafs    , 1917-11-22, {#013E7F ;#FFFFFF							})[Air Canada Centre	    , Mike Babcock		, TMP]\n"
			+ "Team(Vancouver Canucks	    , 1945	, {#003E7e ;#008852 ;#ADAEB2 ;#FFFFFF			})[Rogers Arena				, Willie Desjardins	, VCP]\n"
			+ "Team(Washington Capitals    , 1974	, {#C60C30 ;#002147 ;#FFFFFF					})[Verizon Center			, Barry Trotz		, WCP]\n"
			+ "Team(Winnipeg Jets			, 1999	, {#002D62 ;#006EC8 ;#8D8D8F ;#FFFFFF			})[MTS Centre				, Paul Maurice		, WJP]\n"
			+ "}";
	// </editor-fold>

	private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("hockeyPU");

	public static EntityManager createEntityManager() {
		return emf.createEntityManager();
	}

	public static void close() {
		emf.close();
	}

	public static void populateWithSampleData() {
		League league = createLeague();
		createSeason(league);
		createOfficials(league);
		HashMap<String, Arena> arenas = createArenas();
		HashMap<String, Coach> coachs = createCoachs();
		HashMap<String, Team> teams = createTeams(league, arenas, coachs);
		createPlayers(teams);
		createGames(league);
	}

	private static League createLeague() {
		LeagueDAO ldao = new LeagueDAO(createEntityManager());
		League l = new League();
		l.setName("National Hockey League");
		l.setAbrangence("North America");
		l.setSince(new GregorianCalendar(1917, 11 - 1, 26));
		ldao.create(l);
		return l;
	}

	private static void createSeason(League l) {
		SeasonDAO sdao = new SeasonDAO(createEntityManager());
		Season s = new Season();
		s.setStartDate(new GregorianCalendar(2015, 10 - 1, 7));
		s.setEndDate(new GregorianCalendar(2016, 4 - 1, 9));
		sdao.create(s);
		l.setSeasons(Arrays.asList(s));
		new LeagueDAO(createEntityManager()).update(l);
	}

	private static void createOfficials(League l) {
		OfficialDAO odao = new OfficialDAO(createEntityManager());
		List<Official> list = new ArrayList<>(10);
		for (String line : officialsData.split("\n")) {
			if (!line.startsWith("Official(")) {
				continue;
			}
			Official o = new Official();
			String[] oData = line.substring(line.indexOf('(') + 1, line.indexOf(')')).split(",");
			o.setName(oData[1].trim());
			o.setAge(Integer.parseInt(oData[2].trim()));
			if (oData[3].contains("referee")) {
				o.setPosition(OfficialPosition.referee);
			} else {
				o.setPosition(OfficialPosition.linesman);
			}
			o.getLeagues().add(l);
			odao.create(o);
			list.add(o);
		}
		LeagueDAO ldao = new LeagueDAO(createEntityManager());
		l.setOfficials(list);
		ldao.update(l);
	}

	private static HashMap<String, Arena> createArenas() {
		ArenaDAO adao = new ArenaDAO(createEntityManager());
		HashMap<String, Arena> arenas = new HashMap<>(31);
		for (String line : arenasData.split("\n")) {
			if (!line.startsWith("Arena(")) {
				continue;
			}
			Arena a = new Arena();
			String[] aData = line.substring(line.indexOf('(') + 1, line.indexOf(')')).split(",");
			a.setName(aData[0].trim());
			a.setLeng(Double.parseDouble(aData[1].trim()));
			a.setWidth(Double.parseDouble(aData[2].trim()));
			adao.create(a);
			arenas.put(a.getName(), a);
		}
		return arenas;
	}

	private static HashMap<String, Coach> createCoachs() {
		CoachDAO cdao = new CoachDAO(createEntityManager());
		HashMap<String, Coach> coaches = new HashMap<>(31);
		for (String line : coachsData.split("\n")) {
			if (!line.startsWith("Coach(")) {
				continue;
			}
			Coach c = new Coach();
			String[] cData = line.substring(line.indexOf('(') + 1, line.indexOf(')')).split(",");
			c.setName(cData[1].trim());
			c.setAge(Integer.parseInt(cData[2].trim()));
			cdao.create(c);
			coaches.put(c.getName(), c);
		}
		return coaches;
	}

	private static HashMap<String, Team> createTeams(League l, HashMap<String, Arena> as, HashMap<String, Coach> cs) {
		TeamDAO tdao = new TeamDAO(createEntityManager());
		CoachDAO cdao = new CoachDAO(createEntityManager());
		HashMap<String, Team> teams = new HashMap<>(31);
		for (String line : teamsData.split("\n")) {
			if (!line.startsWith("Team(")) {
				continue;
			}
			Team t = new Team();
			String[] tData = line.substring(line.indexOf('(') + 1, line.indexOf(')')).split(",");
			String[] tRefs = line.substring(line.indexOf('[') + 1, line.indexOf(']')).split(",");
			t.setName(tData[0].trim());
			if (tData[1].contains("-")) {
				String[] date = tData[1].trim().split("-");
				t.setSince(new GregorianCalendar(Integer.parseInt(date[0]), Integer.parseInt(date[1]) - 1, Integer.parseInt(date[2])));
			} else {
				t.setSince(new GregorianCalendar(Integer.parseInt(tData[1].trim()), 0, 1));
			}
			for (String color : tData[2].substring(tData[2].indexOf('{') + 1, tData[2].indexOf('}')).split(";")) {
				t.getColors().add(Color.decode(color.trim()));
			}
			t.setHome(as.get(tRefs[0].trim()));
			Coach c = cs.get(tRefs[1].trim());
			t.setCoach(c);
			tdao.create(t);
			c.setTeam(t);
			cdao.update(c);
			teams.put(tRefs[2].trim(), t);
		}
		List<Team> list = new ArrayList<>(teams.values());
		LeagueDAO ldao = new LeagueDAO(createEntityManager());
		l.setTeams(list);
		ldao.update(l);
		return teams;
	}

	private static void createPlayers(HashMap<String, Team> teams) {
		PlayerDAO pdao = new PlayerDAO(createEntityManager());
		TeamDAO tdao = new TeamDAO(createEntityManager());
		String team = "";
		for (String line : playerData.split("\n")) {
			if (line.contains("{")) {
				team = line.trim().substring(0, 3);
			} else if (line.contains("}")) {
				if (teams.containsKey(team)) {
					teams.get(team).setPlayers(new ArrayList<>(teams.get(team).getPlayers()));
					tdao.update(teams.get(team));
				}
				team = "";
			} else if (line.startsWith("Player(") && teams.containsKey(team)) {
				Team t = teams.get(team);
				Player p = new Player();
				String[] pData = line.substring(line.indexOf('(') + 1, line.indexOf(')')).split(",");
				p.setName(pData[1].trim());
				p.setAge(Integer.parseInt(pData[2].trim()));
				p.setNumber(Integer.parseInt(pData[4].trim()));
				switch (pData[3].trim()) {
					case "LW":
						p.setPosition(Position.leftWing);
						break;
					case "RW":
						p.setPosition(Position.rightWing);
						break;
					case "C":
						p.setPosition(Position.center);
						break;
					case "D":
						p.setPosition(Position.defensemen);
						break;
					case "G":
						p.setPosition(Position.goalie);
						break;
				}
				pdao.create(p);
				t.getPlayers().add(p);
			}
		}
	}

	private static void createGames(League l) {
		Season season = l.getSeasons().get(0);
		Team homeTeam = l.getTeams().get(0);
		Team opposingTeam = l.getTeams().get(1);
		GameDAO gdao = new GameDAO(createEntityManager());
		Game g = new Game();
		g.setHomeTeam(homeTeam);
		g.setOpposingTeam(opposingTeam);
		List<Official> officials = new ArrayList<>(5);
		officials.add(l.getOfficials().get(0));
		officials.add(l.getOfficials().get(1));
		officials.add(l.getOfficials().get(l.getOfficials().size() - 1));
		officials.add(l.getOfficials().get(l.getOfficials().size() - 2));
		g.setOfficials(officials);
		GamePeriodDAO gpdao = new GamePeriodDAO(createEntityManager());
		GamePeriod period = new GamePeriod();
		period.setStartTime(new GregorianCalendar(2015, 10, 3, 12, 0));
		period.setEndTime(new GregorianCalendar(2015, 10, 3, 12, 20));
		Penalty penalty = new Penalty();
		penalty.setType(PenaltyType.boarding);
		penalty.setOffended(opposingTeam.getPlayers().get(0));
		penalty.setOffender(homeTeam.getPlayers().get(0));
		penalty.setServed(homeTeam.getPlayers().get(0));
		penalty.setTimeEvent(new GregorianCalendar(2015, 10, 3, 12, 1));
		PenaltyDAO penaltyDAO = new PenaltyDAO(createEntityManager());
		penaltyDAO.create(penalty);
		Stoppage stoppage = new Stoppage();
		stoppage.setReason(Reason.penalty);
		stoppage.setTimeEvent(new GregorianCalendar(2015, 10, 3, 12, 1));
		StoppageDAO stoppageDAO = new StoppageDAO(createEntityManager());
		stoppageDAO.create(stoppage);
		Substitution substitution = new Substitution();
		substitution.setTimeEvent(new GregorianCalendar(2015, 10, 3, 12, 2));
		substitution.setOut(homeTeam.getPlayers().get(0));
		substitution.setIn(homeTeam.getPlayers().get(1));
		SubstitutionDAO substitutionDAO = new SubstitutionDAO(createEntityManager());
		substitutionDAO.create(substitution);
		Goal goal = new Goal();
		goal.setTimeEvent(new GregorianCalendar(2015, 10, 3, 12, 1, 10));
		goal.setPlayer(opposingTeam.getPlayers().get(0));
		goal.setTeam(opposingTeam);
		GoalDAO goalDAO = new GoalDAO(createEntityManager());
		goalDAO.create(goal);
		PenaltyShot penaltyShot = new PenaltyShot();
		penaltyShot.setTimeEvent(new GregorianCalendar(2015, 10, 3, 12, 1, 8));
		penaltyShot.setPlayer(opposingTeam.getPlayers().get(0));
		penaltyShot.setPosition(StartPosition.circleNE);
		PenaltyShotDAO penaltyShotDAO = new PenaltyShotDAO(createEntityManager());
		penaltyShotDAO.create(penaltyShot);
		Faceoff faceoff = new Faceoff();
		faceoff.setTimeEvent(new GregorianCalendar(2015, 10, 3, 12, 0));
		faceoff.setPosition(StartPosition.middleCircle);
		faceoff.getPlayers().add(homeTeam.getPlayers().get(1));
		faceoff.getPlayers().add(opposingTeam.getPlayers().get(1));
		FaceoffDAO faceoffDAO = new FaceoffDAO(createEntityManager());
		faceoffDAO.create(faceoff);
		period.getEvents().add(penalty);
		period.getEvents().add(stoppage);
		period.getEvents().add(substitution);
		period.getEvents().add(goal);
		period.getEvents().add(penaltyShot);
		period.getEvents().add(faceoff);
		gpdao.create(period);
		g.setEndTime(period.getEndTime());
		g.setStartTime(period.getStartTime());
		g.getPeriods().add(period);
		gdao.create(g);
		SeasonDAO seasonDAO = new SeasonDAO(createEntityManager());
		season.setGames(Arrays.asList(g));
		seasonDAO.update(season);
	}

	private DataSource() {
	}
}
