package model.dao;

import javax.persistence.EntityManager;
import model.Penalty;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public class PenaltyDAO extends GenericDAO<Penalty, Long> {

	public PenaltyDAO(EntityManager em) {
		super(em);
	}

}
