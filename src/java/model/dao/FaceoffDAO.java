package model.dao;

import javax.persistence.EntityManager;
import model.Faceoff;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public class FaceoffDAO extends GenericDAO<Faceoff, Long> {

	public FaceoffDAO(EntityManager em) {
		super(em);
	}

}
