package model.dao;

import javax.persistence.EntityManager;
import model.Substitution;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public class SubstitutionDAO extends GenericDAO<Substitution, Long> {

	public SubstitutionDAO(EntityManager em) {
		super(em);
	}

}
