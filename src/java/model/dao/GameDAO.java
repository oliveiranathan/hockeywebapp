package model.dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import model.Game;
import model.GamePeriod;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public class GameDAO extends GenericDAO<Game, Long> {

	public GameDAO(EntityManager em) {
		super(em);
	}

	public Game findGameOf(GamePeriod gamePeriod) {
		Query query = em.createQuery("SELECT g FROM Game AS g WHERE :period MEMBER OF g.periods");
		query.setParameter("period", gamePeriod);
		try {
			return (Game) query.getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
	}
}
