package model.dao;

import javax.persistence.EntityManager;
import model.Person;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public class PersonDAO extends GenericDAO<Person, Long> {

	public PersonDAO(EntityManager em) {
		super(em);
	}

}
