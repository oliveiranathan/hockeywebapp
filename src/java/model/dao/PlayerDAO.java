package model.dao;

import javax.persistence.EntityManager;
import model.Player;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public class PlayerDAO extends GenericDAO<Player, Long> {

	public PlayerDAO(EntityManager em) {
		super(em);
	}

}
