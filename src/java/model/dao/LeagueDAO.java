package model.dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import model.League;
import model.Season;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public class LeagueDAO extends GenericDAO<League, String> {

	public LeagueDAO(EntityManager em) {
		super(em);
	}

	public League findLeagueOf(Season season) {
		Query query = em.createQuery("SELECT l FROM League AS l WHERE :season MEMBER OF l.seasons");
		query.setParameter("season", season);
		try {
			return (League) query.getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
	}
}
