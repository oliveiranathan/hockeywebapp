package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@Entity
public class Faceoff extends Event implements Serializable {

	private static final long serialVersionUID = 1L;

	@OneToMany
	private List<Player> players = new ArrayList<>(10);
	@Enumerated(EnumType.STRING)
	private StartPosition position;

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<Player> getPlayers() {
		return players;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	public StartPosition getPosition() {
		return position;
	}

	public void setPosition(StartPosition position) {
		this.position = position;
	}

	@Override
	public String toString() {
		return super.toString() + ": Face-off@" + position.getValue() + ": " + players.get(0).getName() + "-" + players.get(1).getName();
	}
}
