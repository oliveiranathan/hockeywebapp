package model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@Entity
public class Arena implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private String name;
	private double leng;
	private double width;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLeng() {
		return leng;
	}

	public void setLeng(double leng) {
		this.leng = leng;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}
}
