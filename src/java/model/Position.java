package model;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public enum Position {

	goalie("Goalie"),
	defensemen("Defensemen"),
	rightWing("Right Wing"),
	leftWing("Left Wing"),
	center("Center");

	private String value;

	private Position(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
