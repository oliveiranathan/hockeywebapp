package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToMany;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
@Entity
public class Official extends Person implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManyToMany
	private List<League> leagues = new ArrayList<>(10);
	@Enumerated(EnumType.STRING)
	private OfficialPosition position;

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<League> getLeagues() {
		return leagues;
	}

	@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
	public void setLeagues(List<League> leagues) {
		this.leagues = leagues;
	}

	public OfficialPosition getPosition() {
		return position;
	}

	public void setPosition(OfficialPosition position) {
		this.position = position;
	}
}
