package model;

/**
 *
 * @author Nathan Oliveira <oliveiranathan at gmail.com>
 */
public enum TieBreakingMethod {

	fiveMin4on4("Five minutes, 4-on-4"),
	twentyMin5on5("Twenty minutes, 5-on-5"),
	shootout("Shootout"),
	suddenDeathShootout("Sudden-death shootout");

	private String value;

	private TieBreakingMethod(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
